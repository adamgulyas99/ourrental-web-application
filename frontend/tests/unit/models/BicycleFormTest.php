<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Bicycle;
use common\tests\UnitTester;
use frontend\models\BicycleForm;

class BicycleFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $bicycleForm = new BicycleForm();

        $bicycleForm->name = 'Cím';
        $this->assertTrue($bicycleForm->validate('name'));

        $bicycleForm->name = null;
        $this->assertFalse($bicycleForm->validate('name'));

        $bicycleForm->description = 'Cím';
        $this->assertTrue($bicycleForm->validate('description'));

        $bicycleForm->description = null;
        $this->assertFalse($bicycleForm->validate('description'));

        $bicycleForm->brand = 'Cím';
        $this->assertTrue($bicycleForm->validate('brand'));

        $bicycleForm->brand = null;
        $this->assertFalse($bicycleForm->validate('brand'));

        $bicycleForm->type = Bicycle::TYPE_URBAN;
        $this->assertTrue($bicycleForm->validate('type'));

        $bicycleForm->type = null;
        $this->assertFalse($bicycleForm->validate('type'));

        $bicycleForm->gender = Bicycle::GENDER_UNISEX;
        $this->assertTrue($bicycleForm->validate('gender'));

        $bicycleForm->gender = null;
        $this->assertFalse($bicycleForm->validate('gender'));

        $bicycleForm->speed = Bicycle::SPEED_MULTIPLE;
        $this->assertTrue($bicycleForm->validate('speed'));

        $bicycleForm->speed = null;
        $this->assertFalse($bicycleForm->validate('speed'));

        $bicycleForm->wheelSize = 123456;
        $this->assertTrue($bicycleForm->validate('wheelSize'));

        $bicycleForm->wheelSize = 'asd12345';
        $this->assertFalse($bicycleForm->validate('wheelSize'));
    }

    public function testFillTo()
    {
        $bicycleForm = new BicycleForm();

        $this->assertInstanceOf(Bicycle::class, $bicycleForm->fillTo(new Bicycle()));
    }
}