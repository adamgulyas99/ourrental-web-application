<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\tests\UnitTester;
use frontend\models\AdvertContact;
use frontend\models\AdvertContactForm;

class AdvertContactFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $advertContactForm = new AdvertContactForm();

        $advertContactForm->name = 'asd12345';
        $this->assertTrue($advertContactForm->validate('name'));

        $advertContactForm->name = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        $advertContactForm->name .= 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        $advertContactForm->name .= 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        $advertContactForm->name .= 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        $this->assertFalse($advertContactForm->validate('name'));

        $advertContactForm->email = 'ourrental.test@gmail.com';
        $this->assertTrue($advertContactForm->validate('email'));

        $advertContactForm->email = 'asd12345';
        $this->assertFalse($advertContactForm->validate('email'));

        $advertContactForm->message = 'asd12345';
        $this->assertTrue($advertContactForm->validate('message'));
    }

    public function testFillTo()
    {
        /** @var AdvertContact $advertContact */
        $advertContact = AdvertContact::find()->orderBy('id DESC')->one();
        $advertContactForm = new AdvertContactForm();

        $this->assertInstanceOf(AdvertContact::class, $advertContactForm->fillTo($advertContact, $advertContact->advert_id));
    }
}