<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Car;
use common\tests\UnitTester;
use frontend\models\CarForm;

class CarFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $carForm = new CarForm();

        $carForm->ageGroup = 2018;
        $this->assertTrue($carForm->validate('ageGroup'));

        $carForm->ageGroup = 'asdasd';
        $this->assertFalse($carForm->validate('ageGroup'));

        $carForm->airConditioner = true;
        $this->assertTrue($carForm->validate('airConditioner'));

        $carForm->brand = 'Volvo';
        $this->assertTrue($carForm->validate('brand'));

        $carForm->brand = null;
        $this->assertFalse($carForm->validate('brand'));

        $carForm->description = 'Leírás';
        $this->assertTrue($carForm->validate('description'));

        $carForm->description = null;
        $this->assertFalse($carForm->validate('description'));

        $carForm->name = 'Leírás';
        $this->assertTrue($carForm->validate('name'));

        $carForm->name = null;
        $this->assertFalse($carForm->validate('name'));

        $carForm->type = Car::TYPE_SUV;
        $this->assertTrue($carForm->validate('type'));

        $carForm->type = null;
        $this->assertFalse($carForm->validate('type'));
    }

    public function testFillTo()
    {
        $carForm = new CarForm();

        $this->assertInstanceOf(Car::class, $carForm->fillTo(new Car()));
    }
}