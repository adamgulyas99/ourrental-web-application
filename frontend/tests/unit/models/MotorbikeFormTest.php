<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Motorbike;
use common\tests\UnitTester;
use frontend\models\MotorbikeForm;

class MotorbikeFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $motorbikeForm = new MotorbikeForm();

        $motorbikeForm->brand = 'Harvey';
        $this->assertTrue($motorbikeForm->validate('brand'));

        $motorbikeForm->brand = null;
        $this->assertFalse($motorbikeForm->validate('brand'));

        $motorbikeForm->description = 'asdasd';
        $this->assertTrue($motorbikeForm->validate('description'));

        $motorbikeForm->description = null;
        $this->assertFalse($motorbikeForm->validate('description'));

        $motorbikeForm->name = 'asdasd';
        $this->assertTrue($motorbikeForm->validate('name'));

        $motorbikeForm->name = null;
        $this->assertFalse($motorbikeForm->validate('name'));

        $motorbikeForm->type = Motorbike::TYPE_ENDURO;
        $this->assertTrue($motorbikeForm->validate('type'));

        $motorbikeForm->type = null;
        $this->assertFalse($motorbikeForm->validate('type'));
    }

    public function testFillTo()
    {
        $motorbikeForm = new MotorbikeForm();

        $this->assertInstanceOf(Motorbike::class, $motorbikeForm->fillTo(new Motorbike()));
    }
}
