<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\tests\UnitTester;
use frontend\models\AdvertContact;
use yii\db\ActiveQuery;

class AdvertContactTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $advertContact = new AdvertContact();

        $advertContact->advert_id = 1;
        $this->assertTrue($advertContact->validate('advert_id'));

        $advertContact->advert_id = 'asd12345';
        $this->assertFalse($advertContact->validate('advert_id'));

        $advertContact->name = 'asd12345';
        $this->assertTrue($advertContact->validate('name'));

        $advertContact->name = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        $advertContact->name .= 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        $advertContact->name .= 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        $advertContact->name .= 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        $this->assertFalse($advertContact->validate('name'));

        $advertContact->email = 'ourrental.test@gmail.com';
        $this->assertTrue($advertContact->validate('email'));

        $advertContact->email = 'asd12345';
        $this->assertFalse($advertContact->validate('email'));

        $advertContact->message = 'asd12345';
        $this->assertTrue($advertContact->validate('message'));

        $advertContact->message = null;
        $this->assertFalse($advertContact->validate('message'));

        $advertContact->create_time = '2020-01-01 10:00:00';
        $this->assertTrue($advertContact->validate('create_time'));
    }

    public function testRelations()
    {
        $advertContact = new AdvertContact();

        $this->assertInstanceOf(ActiveQuery::class, $advertContact->getAdvert());
    }
}