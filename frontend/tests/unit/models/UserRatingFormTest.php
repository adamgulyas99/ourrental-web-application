<?php
namespace frontend\unit\models;

use Codeception\Test\Unit;
use common\models\UserRating;
use common\tests\UnitTester;
use frontend\models\UserRatingForm;

class UserRatingFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $userRatingForm = new UserRatingForm();

        $userRatingForm->createUserId = 1;
        $this->assertTrue($userRatingForm->validate('createUserId'));

        $userRatingForm->createUserId = null;
        $this->assertFalse($userRatingForm->validate('createUserId'));

        $userRatingForm->advertId = 2;
        $this->assertTrue($userRatingForm->validate('advertId'));

        $userRatingForm->advertId = null;
        $this->assertFalse($userRatingForm->validate('advertId'));

        $userRatingForm->rating = 5;
        $this->assertTrue($userRatingForm->validate('rating'));

        $userRatingForm->rating = null;
        $this->assertFalse($userRatingForm->validate('rating'));
    }

    public function testFillTo()
    {
        $userRatingForm = new UserRatingForm();

        $this->assertInstanceOf(UserRating::class, $userRatingForm->fillTo(new UserRating()));
    }

    public function testFillFrom()
    {
        $userRatingForm = new UserRatingForm();

        $this->assertInstanceOf(UserRatingForm::class, $userRatingForm->fillFrom(new UserRating()));
    }
}