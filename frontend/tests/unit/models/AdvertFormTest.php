<?php
namespace frontend\unit\models;

use Codeception\Test\Unit;
use common\models\Advert;
use common\models\Bicycle;
use common\models\User;
use common\tests\UnitTester;
use frontend\models\AdvertForm;

class AdvertFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $advertForm = new AdvertForm();

        $advertForm->title = 'Cím';
        $this->assertTrue($advertForm->validate('title'));

        $advertForm->title = 123456;
        $this->assertFalse($advertForm->validate('title'));

        $advertForm->description = 'Leírás';
        $this->assertTrue($advertForm->validate('description'));

        $advertForm->description = 123456;
        $this->assertFalse($advertForm->validate('description'));

        $advertForm->type = Advert::TYPE_BICYCLE;
        $this->assertTrue($advertForm->validate('type'));

        $advertForm->type = null;
        $this->assertFalse($advertForm->validate('type'));

        $advertForm->rentalCity = 'Város';
        $this->assertTrue($advertForm->validate('rentalCity'));

        $advertForm->rentalCity = 123456;
        $this->assertFalse($advertForm->validate('rentalCity'));

        $advertForm->rentalPrice = '123456';
        $this->assertTrue($advertForm->validate('rentalPrice'));

        $advertForm->rentalPrice = 123456;
        $this->assertFalse($advertForm->validate('rentalPrice'));

        $advertForm->rentalStartTime = '2020-02-03';
        $this->assertTrue($advertForm->validate('rentalStartTime'));

        $advertForm->rentalStartTime = 123456;
        $this->assertFalse($advertForm->validate('rentalStartTime'));

        $advertForm->rentalEndTime = '2020-02-03';
        $this->assertTrue($advertForm->validate('rentalEndTime'));

        $advertForm->rentalEndTime = 123456;
        $this->assertFalse($advertForm->validate('rentalEndTime'));
    }

    public function testFillFromVehicle()
    {
        /** @var Bicycle $vehicle */
        $vehicle = Bicycle::find()->orderBy('id DESC')->one();
        $advertForm = new AdvertForm();

        $this->assertInstanceOf(AdvertForm::class, $advertForm->fillFromVehicle($vehicle, Advert::TYPE_BICYCLE));
    }

    public function testFillRentalAttributes()
    {
        $advertForm = new AdvertForm();
        $params = [
            'rentalPrice' => 12345,
            'rentalCity' => 'Szeged',
            'rentalStartTime' => '2020-01-01 10:00:00',
            'rentalEndTime' => '2020-01-01 10:00:00',
        ];

        $this->assertInstanceOf(AdvertForm::class, $advertForm->fillRentalAttributes($params));
    }

    public function testFillTo()
    {
        /** @var Advert $advert */
        /** @var User $user */

        $advert = Advert::find()->orderBy('id DESC')->one();
        $user = User::find()->one();
        $advertForm = new AdvertForm();

        $this->assertInstanceOf(Advert::class, $advertForm->fillTo($advert, $user));
    }

    public function testItemAlias()
    {
        $this->assertFalse(AdvertForm::itemAlias('randomCol'));
        $this->assertIsArray(AdvertForm::itemAlias('type'));
        $this->assertIsString(AdvertForm::itemAlias('type', Advert::TYPE_BICYCLE));
    }
}