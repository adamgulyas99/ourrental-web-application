<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\tests\UnitTester;
use frontend\models\ContactForm;

class ContactFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $contactForm = new ContactForm();

        $contactForm->name = 'Cím';
        $this->assertTrue($contactForm->validate('name'));

        $contactForm->name = null;
        $this->assertFalse($contactForm->validate('name'));

        $contactForm->email = 'ourrental.test@gmail.com';
        $this->assertTrue($contactForm->validate('email'));

        $contactForm->email = null;
        $this->assertFalse($contactForm->validate('email'));

        $contactForm->subject = 'Tárgy';
        $this->assertTrue($contactForm->validate('subject'));

        $contactForm->subject = null;
        $this->assertFalse($contactForm->validate('subject'));

        $contactForm->body = 'Szöveg';
        $this->assertTrue($contactForm->validate('body'));

        $contactForm->body = null;
        $this->assertFalse($contactForm->validate('body'));
    }
}