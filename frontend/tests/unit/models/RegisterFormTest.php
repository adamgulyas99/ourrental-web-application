<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\User;
use common\tests\UnitTester;
use frontend\models\RegisterForm;

class RegisterFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $registerForm = new RegisterForm();

        $registerForm->city = 'Szeged';
        $this->assertTrue($registerForm->validate('city'));

        $registerForm->country = 'HU';
        $this->assertTrue($registerForm->validate('country'));

        $registerForm->country = 123456;
        $this->assertFalse($registerForm->validate('country'));

        $registerForm->email = 'asdasd@gmail.com';
        $this->assertTrue($registerForm->validate('email'));

        $registerForm->email = 123456;
        $this->assertFalse($registerForm->validate('email'));

        $registerForm->gender = User::GENDER_MALE;
        $this->assertTrue($registerForm->validate('gender'));

        $registerForm->gender = 'asdasdasd';
        $this->assertFalse($registerForm->validate('gender'));

        $registerForm->password = 'asdasd';
        $this->assertTrue($registerForm->validate('password'));

        $registerForm->password = null;
        $this->assertFalse($registerForm->validate('password'));

        $registerForm->phone = '06702501212';
        $this->assertTrue($registerForm->validate('phone'));
    }

    public function testFillTo()
    {
        $registerForm = new RegisterForm();

        $this->assertInstanceOf(User::class, $registerForm->fillTo(new User()));
    }
}