<?php
namespace frontend\unit\models;

use Codeception\Test\Unit;
use common\models\Subscription;
use common\tests\UnitTester;
use frontend\models\SubscriptionForm;

class SubscriptionFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $subscriptionForm = new SubscriptionForm();

        $subscriptionForm->endTime = '2020-12-12';
        $this->assertTrue($subscriptionForm->validate('endTime'));

        $subscriptionForm->endTime = null;
        $this->assertFalse($subscriptionForm->validate('endTime'));

        $subscriptionForm->name = 'Cím';
        $this->assertTrue($subscriptionForm->validate('name'));

        $subscriptionForm->name = null;
        $this->assertFalse($subscriptionForm->validate('name'));

        $subscriptionForm->paid = true;
        $this->assertTrue($subscriptionForm->validate('paid'));

        $subscriptionForm->paid = null;
        $this->assertFalse($subscriptionForm->validate('paid'));

        $subscriptionForm->price = 'Tárgy';
        $this->assertTrue($subscriptionForm->validate('price'));

        $subscriptionForm->price = null;
        $this->assertFalse($subscriptionForm->validate('price'));

        $subscriptionForm->startTime = '2020-10-10';
        $this->assertTrue($subscriptionForm->validate('startTime'));

        $subscriptionForm->updateTime = '2020-10-10';
        $this->assertTrue($subscriptionForm->validate('updateTime'));

        $subscriptionForm->userId = 1;
        $this->assertTrue($subscriptionForm->validate('userId'));

        $subscriptionForm->userId = null;
        $this->assertFalse($subscriptionForm->validate('userId'));
    }

    public function testFillFromPaymentParams()
    {
        $subscriptionForm = new SubscriptionForm();
        $params = [
            'type' => 'free-trial',
            'order' => [
                'total' => 25,
                'items' => [
                    0 => [
                        'name' => 'test package',
                    ],
                ],
            ],
        ];

        $this->assertInstanceOf(SubscriptionForm::class, $subscriptionForm->fillFromPaymentParams($params, 1));
    }

    public function testFillTo()
    {
        $subscriptionForm = new SubscriptionForm();

        $this->assertInstanceOf(Subscription::class, $subscriptionForm->fillTo(new Subscription()));
    }
}