$('#modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');

    var modal = $(this);
    modal.find('.modal-body input').val(id);
});

$('#subscription-order-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var type = button.data('type');

    var typeText = '';
    switch (type) {
        case 'free-trial':
            typeText = '14 napos ingyenes csomag';

            break;
        case 'six-months':
            typeText = '6 hónapos csomag';

            break;
        case 'one-year':
            typeText = '1 éves csomag';

            break;
        default:
            break;
    }

    var modal = $(this);
    modal.find('.modal-header h5').text(typeText + ' megrendelés');
    modal.find('.modal-body #package-text').text(typeText);
    modal.find('.modal-body input').val(type);
});