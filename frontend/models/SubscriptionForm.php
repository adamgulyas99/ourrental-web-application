<?php
namespace frontend\models;

use Yii;
use common\models\Subscription;
use yii\base\Model;

class SubscriptionForm extends Model
{
    /** @var string */
    public $endTime;
    /** @var string */
    public $name;
    /** @var bool */
    public $paid;
    /** @var string */
    public $price;
    /** @var string */
    public $startTime;
    /** @var string */
    public $updateTime;
    /** @var int */
    public $userId;

    public function rules(): array
    {
        return [
            ['paid', 'boolean'],
            ['userId', 'integer'],

            [['userId', 'name', 'paid', 'price', 'endTime'], 'required'],
            [['name', 'price'], 'string', 'max' => 255],
            [['startTime', 'endTime', 'updateTime'], 'safe'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'endTime' => Yii::t('app', 'Vége'),
            'name' => Yii::t('app', 'Elnevezés'),
            'paid' => Yii::t('app', 'Fizetett'),
            'price' => Yii::t('app', 'Ár'),
            'startTime' => Yii::t('app', 'Kezdete'),
            'updateTime' => Yii::t('app', 'Módosítás dátuma'),
            'userId' => Yii::t('app', 'Felhasználó ID'),
        ];
    }

    public function fillFromPaymentParams(array $params, int $userId)
    {
        switch ($params['type']) {
            case 'free-trial':
                $this->endTime = date('Y-m-d H:i:s', strtotime('+14 days'));
                $this->price = '0';

                break;
            case 'six-months':
                $this->endTime = date('Y-m-d H:i:s', strtotime('+6 months'));
                $this->price = (string)$params['order']['total'];

                break;
            case 'one-year':
                $this->endTime = date('Y-m-d H:i:s', strtotime('+1 year'));
                $this->price = (string)$params['order']['total'];

                break;
            default:
                break;
        }

        $this->name = $params['order']['items'][0]['name'];
        $this->paid = true;
        $this->startTime = date('Y-m-d H:i:s', time());
        $this->updateTime = date('Y-m-d H:i:s', time());
        $this->userId = $userId;

        return $this;
    }

    public function fillTo(Subscription $subscription): Subscription
    {
        $subscription->end_time = $this->endTime;
        $subscription->name = $this->name;
        $subscription->paid = $this->paid;
        $subscription->price = $this->price;
        $subscription->start_time = $this->startTime;
        $subscription->update_time = $this->updateTime;
        $subscription->user_id = $this->userId;

        return $subscription;
    }
}