<?php
namespace frontend\models;

use Exception;
use Yii;
use common\models\User;
use yii\base\Model;

class RegisterForm extends Model
{
    /** @var string */
    public $city;
    /** @var string */
    public $country;
    /** @var string */
    public $email;
    /** @var int */
    public $gender;
    /** @var string */
    public $password;
    /** @var string */
    public $phone;
    /** @var int */
    public $zipCode;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['city', 'trim'],
            ['city', 'string', 'max' => 255],

            ['country', 'string', 'max' => 3],

            ['email', 'trim'],
            ['email', 'required', 'message' => 'Az email cím mezőt kötelező kitölteni.'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Az email cím használatban van.'],

            ['gender', 'integer'],
            ['gender', 'default', 'value' => 1],
            ['gender', 'required', 'message' => 'Kötelező megadni a nemet.'],
            ['gender', 'filter', 'filter' => 'intval', 'skipOnEmpty' => false],
            ['gender', 'in', 'range' => [1, 2]],

            ['password', 'required', 'message' => 'A jelszó mezőt kötelező kitölteni.'],
            ['password', 'string', 'min' => 6],

            ['phone', 'trim'],
            ['phone', 'string', 'max' => 20],

            ['zipCode', 'trim'],
            ['zipCode', 'integer', 'max' => 8],
            ['zipCode', 'filter', 'filter' => 'intval', 'skipOnEmpty' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'city' => Yii::t('app', 'Város'),
            'country' => Yii::t('app', 'Ország'),
            'email' => Yii::t('app', 'Email'),
            'gender' => Yii::t('app', 'Nem'),
            'password' => Yii::t('app', 'Jelszó'),
            'phone' => Yii::t('app', 'Telefonszám'),
            'zipCode' => Yii::t('app', 'Irányítószám'),
        ];
    }

    /**
     * @param User $user
     * @return User
     * @throws Exception
     */
    public function fillTo(User $user)
    {
        $user->auth_key = Yii::$app->security->generateRandomString();
        $user->city = $this->city;
        $user->country = $this->country;
        $user->email = $this->email;
        $user->gender = (int)$this->gender;
        $user->password = Yii::$app->security->generatePasswordHash($this->password);
        $user->phone = $this->phone;
        $user->status = User::STATUS_ACTIVE;
        $user->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
        $user->zip_code = $this->zipCode;

        return $user;
    }
}