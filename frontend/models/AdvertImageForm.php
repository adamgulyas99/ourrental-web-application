<?php
namespace frontend\models;

use common\models\Advert;
use common\models\AdvertImage;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class AdvertImageForm extends Model
{
    /** @var int */
    public $advertId;
    /** @var string */
    public $name;
    /** @var int */
    public $type;

    /** @var Advert */
    public $advert;
    /** @var UploadedFile */
    public $imageFile;

    public function rules()
    {
        return [
            [['advertId', 'type'], 'required'],
            ['advertId', 'integer'],
            ['name', 'string', 'max' => 255],

            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'advertId' => Yii::t('app', 'Hirdetés ID'),
            'name' => Yii::t('app', 'File név'),
            'type' => Yii::t('app', 'Típus'),
            'imageFile' => Yii::t('app', 'Borítókép feltöltése'),
        ];
    }

    public function fillTo(AdvertImage $advertImage): AdvertImage
    {
        $advertImage->advert_id = $this->advertId;
        $advertImage->name = $this->name;
        $advertImage->type = $this->type;

        return $advertImage;
    }

    public function setAdvert(Advert $advert): Advert
    {
        $this->advertId = $advert->id;
        $this->advert = $advert;

        return $advert;
    }

    public function uploadCover(): bool
    {
        $advertTitle = str_replace(' ', '_', $this->advert->title);
        $this->name = $advertTitle . '_' . time() . '.' . $this->imageFile->extension;
        $this->type = AdvertImage::TYPE_COVER;

        $filePath = '../web/advertUploads/covers/' . $this->name;

        if ($this->validate()) {
            return $this->imageFile->saveAs($filePath);
        }

        return false;
    }
}
