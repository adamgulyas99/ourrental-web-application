<?php
namespace frontend\models;

use common\components\EventHandler;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use common\models\Advert;

/**
 * AdvertContact model
 *
 * @property int $id
 * @property int $advert_id
 * @property string $name
 * @property string $email
 * @property string $message
 * @property string $create_time
 *
 * @property Advert $advert
 */
class AdvertContact extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%advert_contact}}';
    }

    public function rules()
    {
        return [
            ['advert_id', 'integer'],
            ['email', 'email'],
            ['email', 'string', 'max' => 50],
            ['message', 'string', 'max' => 255],
            ['name', 'string', 'max' => 50],

            [['advert_id', 'name', 'email', 'message'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'advert_id' => Yii::t('app', 'Hirdetés ID'),
            'create_time' => Yii::t('app', 'Létrehozás időpontja'),
            'email' => Yii::t('app', 'Email'),
            'message' => Yii::t('app', 'Üzenet'),
            'name' => Yii::t('app', 'Név'),
        ];
    }

    public function init()
    {
        parent::init();

        $this->on(AdvertContact::EVENT_AFTER_INSERT, [EventHandler::class, 'sendAdvertContactEmail']);
    }

    public function getAdvert(): ActiveQuery
    {
        return $this->hasOne(Advert::class, ['id' => 'advert_id']);
    }
}