<?php
namespace frontend\models;

use common\models\Bicycle;
use common\models\Motorbike;
use yii\base\Model;
use Yii;

class MotorbikeForm extends Model
{
    /** @var string */
    public $brand;
    /** @var string */
    public $description;
    /** @var string */
    public $name;
    /** @var int */
    public $type;

    public function rules(): array
    {
        return [
            [['brand', 'description', 'name', 'type'], 'required'],

            [['brand', 'name'], 'trim'],
            [['brand', 'name'], 'string', 'max' => 255],

            ['description', 'string'],
            ['type', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Név'),
            'description' => Yii::t('app', 'Leírás'),
            'brand' => Yii::t('app', 'Márka'),
            'type' => Yii::t('app', 'Típus'),
        ];
    }

    public function fillTo(Motorbike $motorbike): Motorbike
    {
        $motorbike->brand = $this->brand;
        $motorbike->description = $this->description;
        $motorbike->name = $this->name;
        $motorbike->type = $this->type;

        return $motorbike;
    }
}