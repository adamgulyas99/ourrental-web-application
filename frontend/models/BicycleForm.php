<?php
namespace frontend\models;

use common\models\Bicycle;
use Yii;
use yii\base\Model;

class BicycleForm extends Model
{
    /** @var string */
    public $name;
    /** @var string */
    public $description;
    /** @var string */
    public $brand;
    /** @var int */
    public $type;
    /** @var int */
    public $gender;
    /** @var int */
    public $speed;
    /** @var int */
    public $wheelSize;

    public function rules(): array
    {
        return [
            [['name', 'description', 'brand', 'type', 'gender', 'speed'], 'required'],
            [['name', 'brand'], 'trim'],
            [['name', 'brand'], 'string', 'max' => 255],
            ['description', 'string'],
            [['type', 'gender', 'speed', 'wheelSize'], 'integer'],
            ['gender', 'default', 'value' => Bicycle::GENDER_UNISEX],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Név'),
            'description' => Yii::t('app', 'Leírás'),
            'brand' => Yii::t('app', 'Márka'),
            'type' => Yii::t('app', 'Típus'),
            'gender' => Yii::t('app', 'Nem'),
            'speed' => Yii::t('app', 'Sebesség típus'),
            'wheelSize' => Yii::t('app', 'Kerék'),
        ];
    }

    public function fillTo(Bicycle $bicycle): Bicycle
    {
        $bicycle->name = $this->name;
        $bicycle->description = $this->description;
        $bicycle->brand = $this->brand;
        $bicycle->type = $this->type;
        $bicycle->gender = $this->gender;
        $bicycle->speed = $this->speed;
        $bicycle->wheel_size = $this->wheelSize;

        return $bicycle;
    }
}