<?php
namespace frontend\models;

use common\models\Car;
use yii\base\Model;
use Yii;

class CarForm extends Model
{
    /** @var int */
    public $ageGroup;
    /** @var bool */
    public $airConditioner;
    /** @var string */
    public $brand;
    /** @var string */
    public $description;
    /** @var string */
    public $name;
    /** @var int */
    public $type;

    public function rules(): array
    {
        return [
            ['airConditioner', 'boolean'],
            ['description', 'string'],
            [['ageGroup', 'type'], 'integer'],
            ['type', 'default', 'value' => null],

            [['brand', 'description', 'name', 'type'], 'required'],
            [['brand', 'description', 'name'], 'trim'],
            [['brand', 'name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'ageGroup' => Yii::t('app', 'Évjárat'),
            'airConditioner' => Yii::t('app', 'Légkondicionáló'),
            'name' => Yii::t('app', 'Cím'),
            'brand' => Yii::t('app', 'Márka'),
            'description' => Yii::t('app', 'Leírás'),
            'type' => Yii::t('app', 'Típus'),
        ];
    }

    public function fillTo(Car $car): Car
    {
        $car->age_group = (int)$this->ageGroup;
        $car->air_conditioner = $this->airConditioner;
        $car->brand = $this->brand;
        $car->description = $this->description;
        $car->name = $this->name;
        $car->type = $this->type;

        return $car;
    }
}