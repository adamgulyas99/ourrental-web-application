<?php
namespace frontend\models;

use Yii;
use common\models\UserRating;
use yii\base\Model;
use common\models\User;

class UserRatingForm extends Model
{
    /** @var int */
    public $createUserId;
    /** @var int */
    public $advertId;
    /** @var int */
    public $rating;

    public function rules(): array
    {
        return [
            [['createUserId', 'advertId', 'rating'], 'required'],
            [['createUserId', 'advertId', 'rating'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'createUserId' => Yii::t('app', 'Létrehozó felhasználó ID'),
            'advertId' => Yii::t('app', 'Értékelt hirdetés ID'),
            'rating' => Yii::t('app', 'Értékelés'),
        ];
    }

    public function fillTo(UserRating $userRating): UserRating
    {
        $userRating->create_user_id = $this->createUserId;
        $userRating->advert_id = $this->advertId;
        $userRating->rating = $this->rating;

        return $userRating;
    }

    public function fillFrom(UserRating $userRating)
    {
        $this->rating = $userRating->rating;
        $this->advertId = $userRating->advert_id;
        $this->createUserId = $userRating->create_user_id;

        return $this;
    }
}