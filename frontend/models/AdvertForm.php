<?php
namespace frontend\models;

use common\models\Advert;
use common\models\Bicycle;
use common\models\Car;
use common\models\Motorbike;
use Yii;
use yii\base\Model;
use yii\web\User;

class AdvertForm extends Model
{
    /** @var int */
    public $userId;
    /** @var int */
    public $modelId;
    /** @var string */
    public $title;
    /** @var string */
    public $description;
    /** @var int */
    public $type;
    /** @var string */
    public $data;
    /** @var string */
    public $rentalPrice;
    /** @var string */
    public $rentalCity;
    /** @var string */
    public $rentalStartTime;
    /** @var string */
    public $rentalEndTime;

    public function rules(): array
    {
        return [
            ['type', 'integer', 'max' => 3],
            ['type', 'integer', 'max' => 3],
            ['title', 'string', 'max' => 255],

            [['userId', 'modelId', 'type'], 'required'],
            [['userId', 'modelId'], 'integer'],
            [['description', 'data', 'rentalPrice', 'rentalCity', 'rentalStartTime', 'rentalEndTime'], 'string'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'userId' => Yii::t('app', 'Felhasználó ID'),
            'modelId' => Yii::t('app', 'Model ID'),
            'title' => Yii::t('app', 'Cím'),
            'description' => Yii::t('app', 'Leírás'),
            'type' => Yii::t('app', 'Típus'),
            'data' => Yii::t('app', 'Adat'),
            'rentalPrice' => Yii::t('app', 'Bérlet ár'),
            'rentalCity' => Yii::t('app', 'Város'),
            'rentalStartTime' => Yii::t('app', 'Bérlet kezdete'),
            'rentalEndTime' => Yii::t('app', 'Bérlet vége'),
        ];
    }

    /**
     * @param Bicycle|Car|Motorbike $vehicle
     * @param int $type
     * @return $this
     */
    public function fillFromVehicle($vehicle, int $type): AdvertForm
    {
        $this->data = json_encode($vehicle->constructAdditionalData());
        $this->description = $vehicle->description;
        $this->modelId = $vehicle->id;
        $this->title = $vehicle->name;
        $this->type = $type;

        return $this;
    }

    public function fillRentalAttributes(array $params): AdvertForm
    {
        $this->rentalPrice = $params['rentalPrice'];
        $this->rentalCity = $params['rentalCity'];
        $this->rentalStartTime = $params['rentalStartTime'];
        $this->rentalEndTime = $params['rentalEndTime'];

        return $this;
    }

    /**
     * @param Advert $advert
     * @param User $user
     * @return Advert
     */
    public function fillTo(Advert $advert, $user): Advert
    {
        $advert->data = $this->data;
        $advert->deleted = false;
        $advert->description = $this->description;
        $advert->model_id = $this->modelId;
        $advert->title = $this->title;
        $advert->type = $this->type;
        $advert->update_time = date('Y-m-d H:i:s');
        $advert->user_id = $user->id;
        $advert->views = 0;

        $advert->rental_price = $this->rentalPrice;
        $advert->rental_city = $this->rentalCity;
        $advert->rental_start_time = $this->rentalStartTime;
        $advert->rental_end_time = $this->rentalEndTime;

        return $advert;
    }

    /**
     * @param string $col
     * @param null|int $value
     * @return bool|mixed
     */
    public static function itemAlias(string $col, $value = null)
    {
        $result = [
            'type' => [
                Advert::TYPE_BICYCLE => 'Bicikli',
                Advert::TYPE_MOTORBIKE => 'Motor',
                Advert::TYPE_CAR => 'Autó',
            ],
        ];

        if (isset($value)) {
            return isset($result[$col][$value]) ? $result[$col][$value] : false;
        }

        return isset($result[$col]) ? $result[$col] : false;
    }
}