<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

class AdvertContactForm extends Model
{
    /** @var string */
    public $email;
    /** @var string */
    public $message;
    /** @var string */
    public $name;

    public function rules()
    {
        return [
            ['email', 'email'],
            ['email', 'string', 'max' => 50],

            ['message', 'trim'],
            ['message', 'string', 'max' => 255],

            ['name', 'trim'],
            ['name', 'string', 'max' => 50],

            [['name', 'email', 'message'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'message' => Yii::t('app', 'Üzenet'),
            'name' => Yii::t('app', 'Név'),
        ];
    }

    public function fillTo(AdvertContact $advertContact, int $advertId)
    {
        $advertContact->advert_id = $advertId;
        $advertContact->email = $this->email;
        $advertContact->message = $this->message;
        $advertContact->name = $this->name;

        return $advertContact;
    }
}