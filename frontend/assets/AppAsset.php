<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $depends = [
        'yii\web\YiiAsset',
    ];


    public function init()
    {
        $this->css = [
            '/css/site.css',
            '/css/main.css',
        ];

        $this->js = [
            '/js/main.js'
        ];

        parent::init();
    }
}
