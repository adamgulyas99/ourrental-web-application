<?php
/** @noinspection DuplicatedCode */

namespace frontend\controllers;

use common\models\Advert;
use common\models\AdvertImage;
use common\models\CommentForm;
use common\models\Motorbike;
use frontend\models\AdvertContactForm;
use frontend\models\AdvertForm;
use frontend\models\AdvertImageForm;
use frontend\models\MotorbikeForm;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\UploadedFile;

class MotorbikeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $advertForm = new AdvertForm();
        $advertImageForm = new AdvertImageForm();
        $motorbikeForm = new MotorbikeForm();

        if ($motorbikeForm->load(Yii::$app->request->post()) && $motorbikeForm->validate()) {
            $motorbike = $motorbikeForm->fillTo(new Motorbike());

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!$motorbike->save()) {
                    Yii::$app->session->setFlash('danger', 'Sikertelen motor hirdetés létrehozás.');

                    throw new Exception(
                        "Failed to save {$motorbike->name} advert." . json_encode($motorbike->getErrors())
                    );
                }

                $advertForm = $advertForm->fillFromVehicle($motorbike, Advert::TYPE_MOTORBIKE);
                $advertForm->fillRentalAttributes(Yii::$app->request->post('AdvertForm'));
                $advert = $advertForm->fillTo(new Advert(), Yii::$app->user);

                if (!$advert->save()) {
                    Yii::$app->session->setFlash('danger', 'Sikertelen hirdetés létrehozás.');

                    throw new Exception(
                        "Failed to save {$advert->title} advert." . json_encode($advert->getErrors())
                    );
                }

                $advertImageForm->setAdvert($advert);
                $advertImageForm->imageFile = UploadedFile::getInstance($advertImageForm, 'imageFile');

                if (!$advertImageForm->uploadCover()) {
                    Yii::$app->session->setFlash('danger', 'Sikertelen borítókép feltöltés.');

                    throw new Exception("Failed to save cover image with {$advertImageForm->name} name.");
                }

                $advertImage = $advertImageForm->fillTo(new AdvertImage());

                if (!$advertImage->save()) {
                    Yii::$app->session->setFlash('danger', 'Sikertelen borítókép mentés.');

                    throw new Exception("Failed to save advertImage with {$advertImage->name} name.");
                }

                $transaction->commit();

                $advert->trigger(Advert::EVENT_AFTER_CREATE);
                Yii::$app->session->setFlash('success', 'Sikeres motor hirdetés létrehozás!');

                return $this->redirect(['/advert/index']);
            } catch (\Throwable $e) {
                $transaction->rollBack();

                Yii::error($e);
            }
        }

        return $this->render('create', [
            'advertForm' => $advertForm,
            'advertImageForm' => $advertImageForm,
            'motorbikeForm' => $motorbikeForm,
        ]);
    }

    public function actionView(int $advertId)
    {
        $advertContactForm = new AdvertContactForm();
        $commentForm = new CommentForm();

        $advert = null;
        $motorbike = null;

        try {
            $advert = Advert::findById($advertId);
            if (empty($advert)) {
                throw new Exception("Failed to find #{$advertId} Advert." . json_encode($advert->getErrors()));
            }

            $motorbike = Motorbike::findById($advert->model_id);
            if (empty($motorbike)) {
                throw new Exception(
                    "Failed to find #{$advert->model_id} Bicycle." . json_encode($motorbike->getErrors())
                );
            }

            $advert->addView();
            if (!$advert->save()) {
                throw new Exception("Failed to add a view to #{$advert->id} Advert." . json_encode($advert->getErrors()));
            }
        } catch (\Throwable $e) {
            Yii::error($e);
            Yii::$app->session->setFlash('danger', 'Belső hiba történt, kérjük próbáld újra később.');

            $this->redirect('/advert/index');
        }

        return $this->render('view', [
            'advertContactForm' => $advertContactForm,
            'commentForm' => $commentForm,
            'advert' => $advert,
            'motorbike' => $motorbike,
        ]);
    }
}