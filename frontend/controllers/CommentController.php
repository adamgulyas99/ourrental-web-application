<?php
namespace frontend\controllers;

use Yii;
use common\models\Comment;
use common\models\CommentForm;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class CommentController
 */
class CommentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'delete'],
                'rules' => [
                    [
                        'actions' => ['create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $commentForm = new CommentForm();
        if ($commentForm->load(Yii::$app->request->post()) && $commentForm->validate()) {
            $comment = $commentForm->fillTo(new Comment(), (int)Yii::$app->request->post('advertId'));

            try {
                if (!$comment->save()) {
                    throw new Exception(
                        "Failed to save {$comment->subject} comment." . json_encode($comment->getErrors())
                    );
                }

                Yii::$app->session->setFlash('success', 'Sikeres megjegyzés létrehozás.');
            } catch(\Throwable $e) {
                Yii::error($e);
                Yii::$app->session->setFlash('danger', 'Sikertelen megjegyzés létrehozás.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionDelete(int $id)
    {
        $comment = Comment::findById($id);

        try {
            if (empty($comment)) {
                throw new Exception("Failed to find #{$id} comment.");
            }

            $comment->delete();

            Yii::$app->session->setFlash('success', 'Sikeres megjegyzés törlés!');
        } catch (\Throwable $e) {
            Yii::error($e);
            Yii::$app->session->setFlash('danger', 'Sikertelen megjegyzés törlés!');
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
}