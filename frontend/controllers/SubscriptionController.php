<?php
/** @noinspection DuplicatedCode */
/** @noinspection PhpUndefinedFieldInspection */

namespace frontend\controllers;

use common\components\PayPal;
use common\models\Subscription;
use common\models\User;
use frontend\models\SubscriptionForm;
use Yii;
use \yii\db\Exception;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class SubscriptionController
 */
class SubscriptionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['checkout', 'make-payment', 'start-trial'],
                'rules' => [
                    [
                        'actions' => ['checkout', 'make-payment', 'start-trial'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCheckout(string $type)
    {
        $subscription = Subscription::findOne(['user_id' => Yii::$app->user->id]);
        if (!empty($subscription)) {
            Yii::$app->session->setFlash(
                'danger',
                'Ön jelenleg is rendelkezik aktív előfizetői csomaggal. ' .
                'Új előfizetés beállítása érdekében kérjük törölje a meglévő előfizetését, majd próbálja újra.'
            );

            return $this->redirect(Yii::$app->homeUrl);
        }

        $params = [];
        switch ($type) {
            case 'free-trial':
                $params = [
                    'name' => '14 napos ingyenes csomag',
                    'price' => 0,
                ];

                break;
            case 'six-months':
                $params = [
                    'name' => '6 hónapos csomag',
                    'price' => 25,
                ];

                break;
            case 'one-year':
                $params = [
                    'name' => '1 éves csomag',
                    'price' => 45,
                ];

                break;
            default:
                break;
        }

        if ($params['price'] === 0) {
            return $this->redirect(['start-trial', 'type' => $type]);
        }

        $params = PayPal::constructPaymentParams($params);
        Yii::$app->session->set('subscription', json_encode(array_merge($params, ['type' => $type])));

        Yii::$app->payPal->checkOut($params);

        return true;
    }

    public function actionDelete(int $id)
    {
        $subscription = Subscription::findById($id);

        try {
            if (empty($subscription)) {
                throw new Exception("Failed to find #{$id} subscription.");
            }

            $subscription->delete();

            Yii::$app->session->setFlash('success', 'Sikeres előfizetés törlés!');
        } catch (\Throwable $e) {
            Yii::error($e);
            Yii::$app->session->setFlash('danger', 'Sikertelen előfizetés törlés!');
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionMakePayment()
    {
        $subscriptionParams = json_decode(Yii::$app->session->get('subscription'), true);
        Yii::$app->session->remove('subscription');

        $subscriptionForm = new SubscriptionForm();
        $subscriptionForm->fillFromPaymentParams($subscriptionParams, Yii::$app->user->id);
        $subscription = null;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($_GET['success'] === 'false') {
                throw new Exception("The transaction was unsuccessful with #{$_GET['paymentId']} id.");
            }

            if (!$subscriptionForm->validate()) {
                throw new Exception('Invalid SubscriptionForm attributes.');
            }

            $subscription = $subscriptionForm->fillTo(new Subscription());

            if (!$subscription->save()) {
                throw new Exception("Failed to save {$subscriptionForm->name} Subscription. " . json_encode($subscription->getErrors()));
            }

            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();

            Yii::error($e);
            Yii::$app->session->setFlash('danger', 'Belső hiba történt, kérjük próbálja újra később.');
        }

        $params = [
            'order' => [
                'description' => $subscription->name,
                'subtotal' => (int)$subscription->price,
                'shippingCost' => 0,
                'total' => (int)$subscription->price,
                'currency' => 'EUR',
            ],
        ];

        Yii::$app->payPal->processPayment($params);

        Yii::$app->session->setFlash('success', 'Sikeres fizetés. Köszönjük bizalmát!');

        return $this->redirect(Yii::$app->homeUrl);
    }

    public function actionStartTrial(string $type)
    {
        $user = User::findIdentity(Yii::$app->user->id);
        if (!$user->free_trial) {
            Yii::$app->session->setFlash('danger', 'Ön nem használhatja fel a 14 napos ingyenes csomagját.');

            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        $params = [
            'name' => '14 napos ingyenes csomag',
            'price' => 0,
        ];

        $params = PayPal::constructPaymentParams($params);
        $params = array_merge($params, ['type' => $type]);

        $subscriptionForm = new SubscriptionForm();
        $subscriptionForm->fillFromPaymentParams($params, Yii::$app->user->id);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$subscriptionForm->validate()) {
                throw new Exception('Invalid SubscriptionForm attributes.');
            }

            $subscription = $subscriptionForm->fillTo(new Subscription());

            if (!$subscription->save()) {
                throw new Exception("Failed to save {$subscriptionForm->name} Subscription. " . json_encode($subscription->getErrors()));
            }

            $user = User::findIdentity(Yii::$app->user->id);
            $user->free_trial = false;
            $user->update_time = new Expression('NOW()');

            if (!$user->save()) {
                throw new Exception("Failed to update #{$user->id} User. " . json_encode($user->getErrors()));
            }

            $transaction->commit();

            Yii::$app->session->setFlash('success', 'Ön sikeresen aktiválta a 14 napos ingyenes csomagunkat.');
        } catch (\Throwable $e) {
            $transaction->rollBack();

            Yii::error($e);
            Yii::$app->session->setFlash('danger', 'Belső hiba történt, kérjük próbálja újra később.');
        }

        return $this->redirect(Yii::$app->homeUrl);
    }
}