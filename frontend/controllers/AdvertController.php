<?php
namespace frontend\controllers;

use common\models\Motorbike;
use common\models\Subscription;
use common\models\UserRating;
use frontend\models\UserRatingForm;
use Yii;
use common\models\Advert;
use common\models\AdvertImage;
use common\models\Bicycle;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;

class AdvertController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'create-user-rating', 'delete'],
                'rules' => [
                    [
                        'actions' => ['create', 'create-user-rating', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate($advertType = null)
    {
        $subscription = Subscription::findByUserId(Yii::$app->user->id);
        if (empty($subscription)) {
            Yii::$app->session->setFlash('danger', 'A hirdetés feladáshoz rendelkeznie kell előfizetéssel.');

            return $this->redirect('index');
        }

        if (strtotime($subscription->end_time) < time()) {
            try {
                $subscription->delete();

                Yii::$app->session->setFlash('success', 'Előfizetése törlésre került, mivel a közelmúltban lejárt.');

                return $this->redirect('index');
            } catch (\Throwable $e) {
                Yii::error($e);
                Yii::$app->session->setFlash('danger', 'Belső hiba történt, kérjük próbálja újra később.');
            }
        }

        switch ($advertType) {
            case Advert::TYPE_BICYCLE:
                return $this->redirect('/bicycle/create');

                break;
            case Advert::TYPE_CAR:
                return $this->redirect('/car/create');

                break;
            case Advert::TYPE_MOTORBIKE:
                return $this->redirect('/motorbike/create');

                break;
            default:
                Yii::$app->session->setFlash(
                    'danger',
                    'A hirdetés sikeres létrehozásához kérjük válaszd ki a megfelelő hirdetés típust.'
                );
                return $this->redirect('index');

                break;
        }
    }

    public function actionCreateUserRating()
    {
        $userRatingForm = new UserRatingForm();

        if ($userRatingForm->load(Yii::$app->request->post()) && $userRatingForm->validate()) {
            $isUnique = empty(
                UserRating::findByCreateUserIdAndAdvertId(
                    (int)$userRatingForm->createUserId,
                    (int)$userRatingForm->advertId
                )
            );

            try {
                if (!$isUnique) {
                    Yii::$app->session->setFlash('danger', 'Ezt a hirdetést már értékelte korábban.');

                    throw new Exception("UserRating is already sent.");
                }

                $userRating = $userRatingForm->fillTo(new UserRating());

                if (!$userRating->save()) {
                    Yii::$app->session->setFlash('danger', 'Sikertelen hirdetés értékelés.');

                    throw new Exception("Failed to save UserRating." . json_encode($userRating->getErrors()));
                }

                Yii::$app->session->setFlash('success', 'Sikeres hirdetés értékelés.');
            } catch (\Throwable $e) {
                Yii::error($e);
            }
        }

        return $this->redirect('index');
    }

    public function actionDelete(int $id)
    {
        $advert = Advert::findById($id);

        if (!empty($advert)) {
            try {
                $advert->setDeleted();
                if (!$advert->save()) {
                    throw new \Exception("Failed to set deleted #{$advert->id} advert.");
                }

                Yii::$app->session->setFlash('success', 'Sikeres hirdetés törlés!');
            } catch (\Throwable $e) {
                Yii::error($e);
                Yii::$app->session->setFlash('danger', 'Sikertelen hirdetés törlés!');
            }
        }

        return $this->redirect(['index']);
    }

    public function actionIndex()
    {
        $advertQuery = Advert::getActiveAdverts();
        $adverts = Advert::search($advertQuery, Yii::$app->request->get('Advert'))->all();

        $subscription = null;
        if (!Yii::$app->user->isGuest) {
            $subscription = Subscription::findByUserId(Yii::$app->user->id);
        }

        foreach ($adverts as $key => $advert) {
            /** @var Advert $advert */
            if (empty($advert->user->subscriptions)) {
                unset($adverts[$key]);
            }
        }

        $data = [];
        $images = [];
        $models = [];
        $userRatingForms = [];

        foreach ($adverts as $advert) {
            /** @var Advert $advert */
            $data[$advert->id] = json_decode($advert->data);
            $images[$advert->id] = AdvertImage::findByAdvertId($advert->id);

            switch ($advert->type) {
                case Advert::TYPE_BICYCLE:
                    $models[$advert->id] = Bicycle::findOne(['id' => $advert->model_id]);

                    break;
            }

            $userRatingForm = new UserRatingForm();

            if (!Yii::$app->user->isGuest) {
                $userRating = UserRating::findByCreateUserIdAndAdvertId(
                    Yii::$app->user->id,
                    $advert->id
                );

                if (!empty($userRating)) {
                    $userRatingForm->fillFrom($userRating);
                }

                $userRatingForms[$advert->id] = $userRatingForm;
            }
        }

        return $this->render('index', [
            'adverts' => $adverts,
            'data' => $data,
            'images' => $images,
            'models' => $models,
            'searchParams' => Yii::$app->request->get('Advert'),
            'subscription' => $subscription,
            'userRatingForms' => $userRatingForms,
        ]);
    }

    public function actionView(int $id, int $type)
    {
        $advert = Advert::findById($id);
        if (empty($advert->user->subscriptions)) {
            Yii::$app->session->setFlash(
                'danger',
                'A hirdetés tulajdonosnak nincs aktív előfizetése, így a hirdetése nem tekinthető meg.'
            );

            return $this->redirect('index');
        }

        switch ($type) {
            case Advert::TYPE_BICYCLE:
                $this->redirect(['/bicycle/view', 'advertId' => $id]);

                break;
            case Advert::TYPE_CAR:
                $this->redirect(['/car/view', 'advertId' => $id]);

                break;
            case Advert::TYPE_MOTORBIKE:
                $this->redirect(['/motorbike/view', 'advertId' => $id]);

                break;
            default:
                Yii::$app->session->setFlash('danger', 'Belső hiba történt, kérjük próbáld újra később.');
                $this->redirect('index');

                break;
        }
    }
}