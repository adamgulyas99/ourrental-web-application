<?php
/** @noinspection DuplicatedCode */

namespace frontend\controllers;

use common\models\AdvertImage;
use common\models\Bicycle;
use common\models\CommentForm;
use frontend\models\AdvertImageForm;
use frontend\models\AdvertContactForm;
use frontend\models\BicycleForm;
use frontend\models\AdvertForm;
use Throwable;
use Yii;
use common\models\Advert;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

class BicycleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $advertForm = new AdvertForm();
        $advertImageForm = new AdvertImageForm();
        $bicycleForm = new BicycleForm();

        if ($bicycleForm->load(Yii::$app->request->post())) {
            $bicycle = $bicycleForm->fillTo(new Bicycle());

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!$bicycle->save()) {
                    Yii::$app->session->setFlash('danger', 'Sikertelen bicikli hirdetés létrehozás.');

                    throw new Exception(
                        "Failed to save {$bicycle->name} advert." . json_encode($bicycle->getErrors())
                    );
                }

                $advertForm = $advertForm->fillFromVehicle($bicycle, Advert::TYPE_BICYCLE);
                $advertForm->fillRentalAttributes(Yii::$app->request->post('AdvertForm'));
                $advert = $advertForm->fillTo(new Advert(), Yii::$app->user);

                if (!$advert->save()) {
                    Yii::$app->session->setFlash('danger', 'Sikertelen hirdetés létrehozás.');

                    throw new Exception(
                        "Failed to save {$advert->title} advert." . json_encode($advert->getErrors())
                    );
                }

                $advertImageForm->setAdvert($advert);
                $advertImageForm->imageFile = UploadedFile::getInstance($advertImageForm, 'imageFile');

                if (!$advertImageForm->uploadCover()) {
                    Yii::$app->session->setFlash('danger', 'Sikertelen borítókép feltöltés.');

                    throw new Exception("Failed to save cover image with {$advertImageForm->name} name.");
                }

                $advertImage = $advertImageForm->fillTo(new AdvertImage());

                if (!$advertImage->save()) {
                    Yii::$app->session->setFlash('danger', 'Sikertelen borítókép mentés.');

                    throw new Exception("Failed to save advertImage with {$advertImage->name} name.");
                }

                $transaction->commit();

                $advert->trigger(Advert::EVENT_AFTER_CREATE);
                Yii::$app->session->setFlash('success', 'Sikeres bicikli hirdetés létrehozás!');

                return $this->redirect(['/advert/index']);
            } catch (Throwable $e) {
                $transaction->rollBack();

                Yii::error($e);
            }
        }

        return $this->render('create', [
            'advertForm' => $advertForm,
            'advertImageForm' => $advertImageForm,
            'bicycleForm' => $bicycleForm,
        ]);
    }

    public function actionView(int $advertId)
    {
        $advertContactForm = new AdvertContactForm();
        $commentForm = new CommentForm();

        $advert = null;
        $bicycle = null;

        try {
            $advert = Advert::findById($advertId);
            if (empty($advert)) {
                throw new Exception("Failed to find #{$advertId} Advert." . json_encode($advert->getErrors()));
            }

            $bicycle = Bicycle::findById($advert->model_id);
            if (empty($bicycle)) {
                throw new Exception("Failed to find #{$advert->model_id} Bicycle." . json_encode($bicycle->getErrors()));
            }

            $advert->addView();
            if (!$advert->save()) {
                throw new Exception("Failed to add a view to #{$advert->id} Advert." . json_encode($advert->getErrors()));
            }
        } catch (Throwable $e) {
            Yii::error($e);
            Yii::$app->session->setFlash('danger', 'Belső hiba történt, kérjük próbáld újra később.');

            $this->redirect('/advert/index');
        }

        return $this->render('view', [
            'advertContactForm' => $advertContactForm,
            'commentForm' => $commentForm,
            'advert' => $advert,
            'bicycle' => $bicycle,
        ]);
    }
}