<?php
namespace frontend\controllers;

use common\components\EventHandler;
use Yii;
use frontend\models\AdvertContact;
use frontend\models\AdvertContactForm;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;

class AdvertContactController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $advertContactForm = new AdvertContactForm();

        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('danger', 'Email küldés csak bejelentkezett felhasználók esetén engedélyezett.');

            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        if ($advertContactForm->load(Yii::$app->request->post()) && $advertContactForm->validate()) {
            $advertContact = $advertContactForm->fillTo(new AdvertContact(), (int)Yii::$app->request->post('advertId'));

            try {
                if (!$advertContact->save()) {
                    throw new Exception(
                        "Failed to save {$advertContactForm->name} AdvertContact. " . json_encode($advertContact->getErrors())
                    );
                }

                Yii::$app->session->setFlash('success', 'Sikeres email kiküldés.');
            } catch (\Throwable $e) {
                Yii::error($e);
                Yii::$app->session->setFlash('danger', 'Sikertelen email kiküldés.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
}