<?php
$config['components']['db'] = [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=127.0.0.1;dbname=ourrental',
    'username' => 'ourrental',
    'password' => 'password',
    'charset' => 'utf8',
];

$config['components']['request'] = [
    'cookieValidationKey' => '125DYDQteAklnqkizmnMXps0eUFgKdz1',
];

if (!YII_ENV_PROD) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return array_merge(require __DIR__ . '/main.php', $config);