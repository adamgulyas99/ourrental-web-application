<?php
/** @var AdvertContactForm $advertContactForm */

use frontend\models\AdvertContactForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
?>
<div class="col-6 card">
    <div class="text-center mt-2">
        <h5>Email küldés</h5>
    </div>

    <?php $form = ActiveForm::begin([
        'action' => '/advert-contact/create',
        'id' => 'form-advert-contact-create',
        'method' => 'post',
        'enableClientValidation' => false,
    ]); ?>

    <?= $form->field($advertContactForm, 'name') ?>
    <?= $form->field($advertContactForm, 'email') ?>
    <?= $form->field($advertContactForm, 'message')->textArea(['rows' => 6]) ?>

    <?= $form->field($advertContactForm, 'advertId')
        ->hiddenInput(['name' => 'advertId', 'value' => Yii::$app->request->get('advertId')])
        ->label(false) ?>

    <div class="form-group text-center">
        <?= Html::submitButton('Küldés', ['class' => 'btn btn-primary', 'name' => 'advert-contact-button']) ?>
    </div>

    <?php ActiveForm::end() ?>
</div>