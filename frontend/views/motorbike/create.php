<?php
/** @var yii\web\View $this */
/** @var ActiveForm $form */
/** @var AdvertForm $advertForm */
/** @var AdvertImageForm $advertImageForm */
/** @var MotorbikeForm $motorbikeForm */

use \common\models\Motorbike;
use frontend\models\AdvertForm;
use frontend\models\AdvertImageForm;
use \frontend\models\MotorbikeForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Motor hirdetés létrehozása';
?>

<?php $form = ActiveForm::begin(['id' => 'create-motor-advert-form']); ?>

<div class="row">
    <div class="col-6">
        <?= $form->field($motorbikeForm, 'name')->textInput() ?>
        <?= $form->field($motorbikeForm, 'description')->textarea() ?>
        <?= $form->field($motorbikeForm, 'brand')->textInput() ?>
        <?= $form->field($motorbikeForm, 'type')->dropDownList(Motorbike::itemAlias('type'), ['prompt' => '']) ?>

        <hr>

        <?= $form->field($advertImageForm, 'imageFile')->fileInput() ?>
    </div>

    <div class="col-6">
        <?= $this->render('/advert/_createRentalData', ['advertForm' => $advertForm, 'form' => $form]) ?>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <hr>

        <div class="form-group text-center">
            <?= Html::submitButton('Létrehozás', ['class' => 'btn btn-primary', 'name' => 'advert-button']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>