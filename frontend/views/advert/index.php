<?php
/** @noinspection PhpUnhandledExceptionInspection */

/** @var View $this */
/** @var Advert[] $adverts */
/** @var array $images */
/** @var array $models */
/** @var array $searchParams */
/** @var Subscription $subscription */
/** @var UserRatingForm[] $userRatingForms */

use common\models\Advert;
use common\models\Subscription;
use dosamigos\leaflet\layers\Marker;
use dosamigos\leaflet\layers\TileLayer;
use dosamigos\leaflet\LeafLet;
use dosamigos\leaflet\types\LatLng;
use dosamigos\leaflet\widgets\Map;
use frontend\models\UserRatingForm;
use OpenCage\Geocoder\Geocoder;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;

$advertCounter = 1;
$newAdvertBtnOptions = [];
if (empty($subscription)) {
    $newAdvertBtnOptions = ['disabled' => true];
}

$this->title = 'Hirdetések listázása';

$center = new LatLng(['lat' => 47.497913, 'lng' => 19.040236]);
$tileLayer = new TileLayer([
    'urlTemplate' => 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png',
    'clientOptions' => [
        'attribution' => 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
        'subdomains' => '1234'
    ]
]);

$leaflet = new LeafLet([
    'center' => $center,
    'tileLayer' => $tileLayer,
    'zoom' => 7,
]);

$geoCoder = new Geocoder('544a4327a0324a6aafa6523a2d73b00b');

$advertCounterByCity = [];
foreach ($adverts as $advert) {
    $geoCoderResult = $geoCoder->geocode($advert->rental_city);

    $latLng = $geoCoderResult['results'][0]['geometry'];
    if (empty($latLng)) {
        continue;
    }

    $advertCounterByCity[$advert->rental_city] = !isset($advertCounterByCity[$advert->rental_city])
        ? 1
        : $advertCounterByCity[$advert->rental_city] + 1;

    $coordinates = new LatLng($latLng);
    $marker = new Marker(['latLng' => $coordinates, 'popupContent' => $advertCounterByCity[$advert->rental_city] . ' hirdetés']);

    $leaflet->addLayer($marker);
}
?>
<?php if (!Yii::$app->user->isGuest) { ?>
    <div class="d-flex justify-content-center mb-4">
        <?= Html::button('Új hirdetés', array_merge([
            'class' => 'btn btn-outline-success w-100',
            'data-target' => '#advert-create-modal',
            'data-toggle' => 'modal',
            'title' => 'Új hirdetés feladásához rendelkeznie kell előfizetéssel.',
        ], $newAdvertBtnOptions)); ?>

        <?= $this->render('_create') ?>
    </div>
<?php } ?>

<?= $this->render('_search', ['searchParams' => $searchParams]) ?>

<div class='mb-2' style="height: 350px; width: 100%;">
    <?= Map::widget(['leafLet' => $leaflet, 'options' => ['style' => 'height: 350px; width: 100%;']]) ?>
</div>

<?php if (!empty($adverts)) { ?>
    <div class="card-deck">
        <?php foreach ($adverts as $advertKey => $advert) { ?>
            <?php if (empty($advert->description)) {
                 continue;
            } ?>

            <?php if ($advertCounter === 2) { ?>
                <div class="row">
            <?php } ?>

            <div class="col-6 mb-4">
                <div class="card">
                    <a href="<?= Url::to(['view', 'id' => $advert->id, 'type' => $advert->type]) ?>">
                        <?php if (!empty($images[$advert->id])) { ?>
                            <?= Html::img('@web/advertUploads/covers/' . $images[$advert->id]->name, [
                                'class' => 'card-img-top',
                                'style' => 'height: 300px',
                                'alt' => 'Hirdetéshez tartozó kép',
                            ]) ?>
                        <?php } else { ?>
                            <?= Html::img('@web/advertUploads/noimg.jpg', [
                                'class' => 'card-img-top',
                                'alt' => 'Hirdetéshez tartozó kép'
                            ]) ?>
                        <?php } ?>
                    </a>

                    <?php if (!Yii::$app->user->isGuest) { ?>
                        <?= $this->render('/userRating/_create', [
                            'advert' => $advert,
                            'userRatingForm' => $userRatingForms[$advert->id],
                        ]) ?>
                    <?php } ?>

                    <div class="card-body mt-4 mb-0 pt-0">
                        <a class="text-decoration-none text-dark" href="<?= Url::to(['view', 'id' => $advert->id, 'type' => $advert->type]) ?>">
                            <h5 class="card-title text-truncate"><?= $advert->title ?></h5>
                        </a>

                        <p class="card-text text-truncate"><?= $advert->description ?></p>
                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-6">
                                <small class="text-muted ml-auto">
                                    <?= 'Létrehozás dátuma: ' . Yii::$app->formatter->asDate($advert->create_time, 'php:Y-m-d') ?>
                                </small>
                            </div>

                            <?php if ($advert->user_id === Yii::$app->user->id) { ?>
                                <div class="col-6 text-right">
                                    <?= Html::button('Törlés', [
                                        'class' => 'btn btn-outline-danger',
                                        'data-target' => '#modal',
                                        'data-toggle' => 'modal',
                                        'data-id' => $advert->id,
                                        'title' => 'Hirdetés törlése',
                                    ]); ?>

                                    <?= $this->render('_delete') ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($advertCounter === 2) { ?>
                </div>
                <?php $advertCounter = 1 ?>
            <?php } ?>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="jumbotron">
        <div class="container">
            <p class="lead">Jelenleg nincs megjelenítendő hirdetés.</p>
        </div>
    </div>
<?php } ?>
