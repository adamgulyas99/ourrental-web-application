<?php
/** @var array $searchParams */

use common\models\Advert;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="d-flex justify-content-center mb-2">
    <button type="button" class="btn btn-info w-100" data-toggle="collapse" data-target="#advertSearch" aria-expanded="<?= !empty($searchParams) ? 'true' : 'false' ?>">Keresés</button>
</div>

<div class="row mb-2">
    <div class="col-12">
        <div class="collapse" id="advertSearch">
            <div class="card card-body">
                <form action="<?= Url::to(['index']) ?>" class="form-horizontal" id="advert-search-form" method="get">
                    <div class="form-group">
                        <div class="row mb-2">
                            <div class="offset-1 col-3 align-self-center text-right">
                                <label for="advertTitle"><?= Advert::instance()->getAttributeLabel('title') ?></label>
                            </div>

                            <div class="col-4">
                                <input type="text" class="form-control" id="advertTitle" name="Advert[title]" value="<?= $searchParams['title'] ?? '' ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row mb-2">
                            <div class="offset-1 col-3 align-self-center text-right">
                                <label for="advertDescription"><?= Advert::instance()->getAttributeLabel('description') ?></label>
                            </div>

                            <div class="col-4">
                                <input type="text" class="form-control" id="advertDescription" name="Advert[description]" value="<?= $searchParams['description'] ?? '' ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row mb-2">
                            <div class="offset-1 col-3 align-self-center text-right">
                                <label for="advertType"><?= Advert::instance()->getAttributeLabel('type') ?></label>
                            </div>

                            <div class="col-4">
                                <?= Html::dropDownList('Advert[type]', $searchParams['type'] ?? null, Advert::itemAlias('type'), ['class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row mb-2">
                            <div class="offset-1 col-3 align-self-center text-right">
                                <label for="rentalPrice"><?= Advert::instance()->getAttributeLabel('rental_price') ?></label>
                            </div>

                            <div class="col-4">
                                <input type="text" class="form-control" id="rentalPrice" name="Advert[rentalPrice]" value="<?= $searchParams['rentalPrice'] ?? '' ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row mb-2">
                            <div class="offset-1 col-3 align-self-center text-right">
                                <label for="rentalCity"><?= Advert::instance()->getAttributeLabel('rental_city') ?></label>
                            </div>

                            <div class="col-4">
                                <input type="text" class="form-control" id="rentalCity" name="Advert[rentalCity]" value="<?= $searchParams['rentalCity'] ?? '' ?>">
                            </div>
                        </div>
                    </div>

                    <?php if (!Yii::$app->user->isGuest) { ?>
                        <div class="form-group">
                            <div class="row mb-2">
                                <div class="offset-1 col-3 align-self-center text-right">
                                    <label for="advertType">Saját hirdetések</label>
                                </div>

                                <div class="col-4">
                                    <?= Html::checkbox('Advert[owned]', isset($searchParams['owned'])) ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="d-flex justify-content-center">
                        <?= Html::input('submit', null, 'Keresés', ['class' => 'btn btn-success']) ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>