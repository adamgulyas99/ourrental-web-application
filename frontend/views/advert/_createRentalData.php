<?php
/** @var yii\web\View $this */
/** @var ActiveForm $form */
/** @var AdvertForm $advertForm */

use frontend\models\AdvertForm;
use yii\bootstrap\ActiveForm;
?>
<?= $form->field($advertForm, 'rentalPrice')->textInput() ?>
<?= $form->field($advertForm, 'rentalCity')->textInput() ?>
<?= $form->field($advertForm, 'rentalStartTime')->textInput(['placeholder' => '2020-01-01 00:00:00']) ?>
<?= $form->field($advertForm, 'rentalEndTime')->textInput(['placeholder' => '2020-01-01 00:00:00']) ?>