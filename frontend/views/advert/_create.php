<?php
/** @var yii\web\View $this */
/** @var ActiveForm $form */
/** @var AdvertForm $advertForm */

use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \frontend\models\AdvertForm;
use yii\helpers\Url;

$this->title = 'Hirdetés létrehozása';
$this->params['breadcrumbs'][] = $this->title;

$advertTypes = AdvertForm::itemAlias('type');
$buttons = Html::button('Tovább', [
    'type' => 'submit',
    'class' => 'btn btn-success',
    'form' => 'advert-create-form',
]);
$buttons .= Html::button('Mégse', [
    'type' => 'button',
    'class' => 'btn btn-outline-secondary',
    'data-dismiss' => 'advert-create-modal',
]);

Modal::begin([
    'id' => 'advert-create-modal',
    'footer' => $buttons,
    'footerOptions' => ['class' => 'justify-content-center'],
    'headerOptions' => ['id' => 'modalHeader'],
    'size' => 'modal-lg',
    'title' => 'Hirdetés létrehozása',
]);
?>

<div class="row">
    <div class="col-12">
        <form action="<?= Url::to(['create']) ?>" class="form-horizontal" id="advert-create-form" method="get">

        <div class="form-group row justify-content-center">
            <?= Html::label('Hirdetés típus:', 'advert-type-select', ['class' => 'col-2 col-form-label']) ?>
            <?= Html::dropDownList(
                'advertType',
                null,
                $advertTypes,
                ['class' => 'form-control col-4', 'id' => 'advert-type-select', 'prompt' => '']) ?>
        </div>

        </form>
    </div>
</div>

<?php Modal::end(); ?>