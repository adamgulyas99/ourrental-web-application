<?php
/** @var string $pageTitle */
?>

<div class="text-center mx-auto">
    <h3 class="text-dark"><strong><?= $pageTitle ?></strong></h3>
</div>