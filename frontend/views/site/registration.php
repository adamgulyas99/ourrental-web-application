<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $registerForm \frontend\models\RegisterForm */

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use common\components\Country;

$this->title = 'Regisztráció';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row justify-content-center">
    <div class="col-5">
        <?php $form = ActiveForm::begin([
            'action' => 'register',
            'id' => 'form-register',
            'method' => 'post',
            'enableClientValidation' => false,
        ]); ?>
            <?= $form->field($registerForm, 'email')->textInput(['autofocus' => true, 'maxlength' => 255]) ?>
            <?= $form->field($registerForm, 'password')->passwordInput() ?>

            <?= $form->field($registerForm, 'gender')->radioList(
                [User::GENDER_MALE => 'Férfi', User::GENDER_FEMALE => 'Nő'],
                [
                    'class' => 'custom-control-inline',
                    'item' => function($index, $label, $name, $checked, $value) {
                        $return = '<label class="modal-radio mr-4">';
                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                        $return .= '<span>' . ucwords($label) . '</span>';
                        $return .= '</label>';

                        return $return;
                    }
                ]
            )->label(false) ?>

            <?= $form->field($registerForm, 'country')->dropDownList(
                Country::getCountriesArray(),
                ['prompt' => 'Kérlek válassz...']
            ) ?>

            <?= $form->field($registerForm, 'zipCode')->textInput(['type' => 'text', 'maxlength' => 8]) ?>
            <?= $form->field($registerForm, 'city')->textInput(['type' => 'text', 'maxlength' => 255]) ?>
            <?= $form->field($registerForm, 'phone')->textInput(['type' => 'text', 'maxlength' => 20]) ?>

            <div class="form-group text-center">
                <?= Html::submitButton('Mentés', ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
