<?php
/* @var yii\web\View $this */
/* @var Subscription $ownedSubscription */
/* @var User $user */

use common\models\Subscription;
use common\models\User;
use yii\bootstrap4\Html;

$this->title = 'Mostantól a járműkölcsönzés házhoz megy!';

$additionalButtonOptions = [];
if (!empty($ownedSubscription)) {
    $additionalButtonOptions = [
        'disabled' => true,
    ];
}

$freeTrialOptions = [];
if (!Yii::$app->user->isGuest && !$user->free_trial) {
    $freeTrialOptions = [
        'disabled' => true,
    ];
}
?>
<div class="row mb-3">
    <div class="col-4">
        <div class="card text-center">
            <div class="card-body">
                <i class="fa fa-gift fa-5x"></i>
                <h2 class="card-title">14 nap ingyen</h2>
                <hr>
                <p class="card-text">
                    Próbálja ki oldalunkat 14 napon keresztül ingyen és kössön üzletet más emberekkel.
                </p>
                <hr>
                <h3>
                    <strong>0 </strong><i class="fa fa-euro-sign"></i>
                </h3>
            </div>

            <div class="card-footer bg-white border-top-0">
                <?= Html::button('Megrendel', array_merge([
                    'class' => 'btn btn-outline-primary rounded-pill',
                    'data-target' => '#subscription-order-modal',
                    'data-toggle' => 'modal',
                    'data-type' => 'free-trial',
                ], $additionalButtonOptions, $freeTrialOptions)); ?>
            </div>
        </div>
    </div>

    <div class="col-4">
        <div class="card text-center">
            <div class="card-body">
                <i class="fa fa-tag fa-5x"></i>
                <h2 class="card-title">6 hónap</h2>
                <hr>
                <p class="card-text">
                    Hirdessen oldalunkon 6 hónapon keresztül és kössön üzletet más emberekkel.
                </p>
                <hr>
                <h3>
                    <strong>25 </strong><i class="fa fa-euro-sign"></i>
                </h3>
            </div>

            <div class="card-footer bg-white border-top-0">
                <?= Html::button('Megrendel', array_merge([
                    'class' => 'btn btn-outline-primary rounded-pill',
                    'data-target' => '#subscription-order-modal',
                    'data-toggle' => 'modal',
                    'data-type' => 'six-months',
                ], $additionalButtonOptions)); ?>
            </div>
        </div>
    </div>

    <div class="col-4">
        <div class="card text-center">
            <div class="card-body">
                <i class="fa fa-tags fa-5x"></i>
                <h2 class="card-title">1 év</h2>
                <hr>
                <p class="card-text">
                    Hirdessen oldalunkon 1 éven keresztül és kössön üzletet más emberekkel.
                </p>
                <hr>
                <h3>
                    <strong>45 </strong><i class="fa fa-euro-sign"></i>
                </h3>
            </div>

            <div class="card-footer bg-white border-top-0">
                <?= Html::button('Megrendel', array_merge([
                    'class' => 'btn btn-outline-primary rounded-pill',
                    'data-target' => '#subscription-order-modal',
                    'data-toggle' => 'modal',
                    'data-type' => 'one-year',
                ], $additionalButtonOptions)); ?>
            </div>
        </div>
    </div>

    <?= $this->render('/subscription/_order') ?>
</div>

<div class="mb-4 text-right text-muted">
    * A fizetés jelenleg csak PayPal fizető rendszerrel lehetséges
</div>

<?php if (!empty($ownedSubscription)) { ?>
    <div class="text-center mt-4">
        <h3>Aktív előfizetői csomag</h3>
    </div>

    <div class="card mt-4">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col-4 text-center">
                    <?php switch ($ownedSubscription->name) {
                        case '14 napos ingyenes csomag':
                            echo '<i class="fa fa-gift fa-5x"></i>';

                            break;
                        case '6 hónapos csomag':
                            echo '<i class="fa fa-tag fa-5x"></i>';

                            break;
                        case '1 éves csomag':
                            echo '<i class="fa fa-tags fa-5x"></i>';

                            break;
                        default:
                            break;
                    } ?>
                </div>

                <div class="col-4 text-center">
                    <h5 class="card-title"><?= $ownedSubscription->name ?></h5>
                </div>

                <div class="col-4 text-right pr-4">
                    <?= Html::button('<i></i>', [
                        'class' => 'far fa-trash-alt border-0 bg-light',
                        'data-target' => '#modal',
                        'data-toggle' => 'modal',
                        'data-id' => $ownedSubscription->id,
                    ]); ?>

                    <?= $this->render('/subscription/_delete') ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>