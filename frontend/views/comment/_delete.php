<?php
/** @var Comment $comment */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Modal;

$buttons = Html::button('Törlés', [
    'type' => 'submit',
    'class' => 'btn btn-danger',
    'form' => 'comment-delete-form',
]);
$buttons .= Html::button('Mégse', [
    'type' => 'button',
    'class' => 'btn btn-outline-secondary',
    'data-dismiss' => 'modal'
]);

Modal::begin([
    'id' => 'modal',
    'footer' => $buttons,
    'footerOptions' => ['class' => 'justify-content-center'],
    'headerOptions' => ['id' => 'modalHeader'],
    'size' => 'modal-lg',
    'title' => 'Megjegyzés törlése',
]);
?>
<form action="<?= Url::to(['/comment/delete']) ?>" class="form-horizontal" id="comment-delete-form" method="get">
    <div class="text-center">
        <p>Biztosan törlöd a kiválasztott megjegyzésed?</p>
    </div>

    <?= Html::hiddenInput('id') ?>
</form>

<?php Modal::end(); ?>