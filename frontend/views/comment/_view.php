<?php
/** @noinspection PhpUndefinedMethodInspection */

/** @var View $this */
/** @var Advert $advert */

use yii\helpers\Html;
use common\models\Comment;

$blockWidth = 'col-6';
if (Yii::$app->user->isGuest) {
    $blockWidth = 'col-12';
}
?>
<div class="<?= $blockWidth ?> border rounded overflow-auto" style="height: 500px;">
    <?php foreach ($advert->comments as $comment) { ?>
        <?php /** @var Comment $comment */?>
        <div class="row justify-content-start border rounded bg-light p-3">
            <div class="col-11">
                <div class="row">
                    <div class="col-12">
                        <b>
                            <?= $comment->subject ?>
                            <span class="badge badge-secondary ml-1"><?= $comment->user->email ?></span>
                        </b>
                    </div>

                    <div class="col-12 pt-3">
                        <p class="ml-3"><?= $comment->content ?></p>
                    </div>
                </div>
            </div>

            <?php if (Yii::$app->user->id === $advert->user_id) { ?>
                <div class="col-1 align-self-center">
                    <?= Html::button('<i></i>', [
                        'class' => 'far fa-trash-alt border-0 bg-light',
                        'data-target' => '#modal',
                        'data-toggle' => 'modal',
                        'data-id' => $comment->id,
                        'title' => 'Megjegyzés törlése',
                    ]); ?>

                    <?= $this->render('_delete') ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
