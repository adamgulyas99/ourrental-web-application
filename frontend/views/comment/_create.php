<?php
/** @var CommentForm $commentForm */

use \common\models\CommentForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
?>
<div class="col-6 card-body align-self-center">
    <div class="text-center mt-2">
        <h5>Új megjegyzés</h5>
    </div>

    <?php $form = ActiveForm::begin([
        'action' => '/comment/create',
        'id' => 'form-comment',
        'method' => 'post',
        'enableClientValidation' => false,
    ]); ?>

    <?= $form->field($commentForm, 'subject')->textInput() ?>
    <?= $form->field($commentForm, 'content')->textarea(['rows' => 5]) ?>

    <?= $form->field($commentForm, 'advertId')
        ->hiddenInput(['name' => 'advertId', 'value' => Yii::$app->request->get('advertId')])
        ->label(false) ?>

    <div class="form-group text-center">
        <?= Html::submitButton('Küldés', ['class' => 'btn btn-primary', 'name' => 'comment-button']) ?>
    </div>

    <?php ActiveForm::end() ?>
</div>
