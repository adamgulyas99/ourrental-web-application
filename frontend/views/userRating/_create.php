<?php
/** @noinspection PhpUnhandledExceptionInspection */

/** @var View $this */
/** @var Advert $advert */
/** @var UserRatingForm $userRatingForm */

use common\models\Advert;
use frontend\models\UserRatingForm;
use yii\bootstrap4\ActiveForm;
use yii\web\View;
use yii2mod\rating\StarRating;

$js = <<<JS
$('form#user-rating-form').on('click', function() {
    this.submit();
});
JS;

$this->registerJs($js);

$form = ActiveForm::begin([
    'id' => 'user-rating-form',
    'action' => '/advert/create-user-rating',
]);

echo $form->field($userRatingForm, 'rating')
    ->widget(StarRating::class, ['options' => ['class' => 'form-control border-0 p-0 pl-3']])
    ->label(false);

echo $form->field($userRatingForm, 'createUserId')->hiddenInput(['value' => Yii::$app->user->id])->label(false);
echo $form->field($userRatingForm, 'advertId')->hiddenInput(['value' => $advert->id])->label(false);

ActiveForm::end();