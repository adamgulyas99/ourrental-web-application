<?php
/** @var yii\web\View $this */
/** @var ActiveForm $form */
/** @var AdvertForm $advertForm */
/** @var AdvertImageForm $advertImageForm */
/** @var CarForm $carForm */

use common\models\Car;
use frontend\models\AdvertForm;
use frontend\models\AdvertImageForm;
use \frontend\models\CarForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Autó hirdetés létrehozása';
?>

<?php $form = ActiveForm::begin(['id' => 'create-motor-advert-form']); ?>

<div class="row">
    <div class="col-6">
        <?= $form->field($carForm, 'name')->textInput() ?>
        <?= $form->field($carForm, 'description')->textarea() ?>
        <?= $form->field($carForm, 'brand')->textInput() ?>
        <?= $form->field($carForm, 'type')->dropDownList(Car::itemAlias('type'), ['prompt' => '']) ?>
        <div class="row">
            <div class="col-6">
                <?= $form->field($carForm, 'ageGroup')->textInput(['maxlength' => 4, 'style'=>'width:100px']) ?>
            </div>
            <div class="col-6 align-self-center text-center">
                <?= $form->field($carForm, 'airConditioner')->checkbox() ?>
            </div>
        </div>

        <hr>

        <?= $form->field($advertImageForm, 'imageFile')->fileInput() ?>
    </div>

    <div class="col-6">
        <?= $this->render('/advert/_createRentalData', ['advertForm' => $advertForm, 'form' => $form]) ?>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <hr>

        <div class="form-group text-center">
            <?= Html::submitButton('Létrehozás', ['class' => 'btn btn-primary', 'name' => 'advert-button']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>