<?php
/** @var yii\web\View $this */
/** @var string $type */

use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

$buttons = Html::button('Megrendelés véglegesítése', [
    'type' => 'submit',
    'class' => 'btn btn-success',
    'form' => 'subscription-order-form',
]);
$buttons .= Html::button('Mégse', [
    'type' => 'button',
    'class' => 'btn btn-outline-secondary',
    'data-dismiss' => 'modal',
]);

Modal::begin([
    'id' => 'subscription-order-modal',
    'footer' => $buttons,
    'footerOptions' => ['class' => 'justify-content-center'],
    'headerOptions' => ['id' => 'modalHeader'],
    'size' => 'modal-lg',
    'title' => 'Csomag megrendelés',
]);
?>

<div class="row">
    <div class="col-12">
        <form action="<?= Url::to(['/subscription/checkout']) ?>" class="form-horizontal" id="subscription-order-form" method="get">
            <div class="row">
                <div class="col-12 text-center">
                    <p>Ön az alábbi csomagot választotta ki megrendelésre: <strong id="package-text"></strong></p>
                </div>

                <div class="col-12 text-center">
                    <p>Véglegesíti megrendelését?</p>
                </div>
            </div>

            <?= Html::hiddenInput('type') ?>
        </form>
    </div>
</div>

<?php Modal::end();