<?php
/** @noinspection PhpUnhandledExceptionInspection */

/** @var View $this */
/** @var AdvertContactForm $advertContactForm */
/** @var CommentForm $commentForm */
/** @var Advert $advert */
/** @var Bicycle $bicycle */

use \common\models\Bicycle;
use \common\models\CommentForm;
use \frontend\models\AdvertContactForm;
use \yii\widgets\DetailView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use yii\web\View;

$this->title = 'Bicikli hirdetés megtekintése';
?>
<div class="card mb-4 w-100 border-0">
    <div class="row no-gutters">
        <div class="col-5 align-self-center">
            <?= Html::img('@web/advertUploads/covers/' . $bicycle->advert->advertCoverImage->name, [
                'class' => 'card-img',
                'alt' => 'Hirdetéshez tartozó kép',
            ]) ?>
        </div>
        <div class="col-7">
            <?= DetailView::widget([
                'model' => $bicycle,
                'attributes' => [
                    'name',
                    'description:html',
                    'brand',
                    [
                        'label' => $bicycle->getAttributeLabel('type'),
                        'value' => Bicycle::itemAlias('type', $bicycle->type),
                    ],
                    [
                        'label' => $bicycle->getAttributeLabel('gender'),
                        'value' => Bicycle::itemAlias('gender', $bicycle->gender),
                    ],
                    [
                        'label' => $bicycle->getAttributeLabel('wheel_size'),
                        'value' => Bicycle::itemAlias('wheelSize', $bicycle->wheel_size),
                    ],
                    [
                        'label' => $bicycle->getAttributeLabel('create_time'),
                        'value' => Yii::$app->formatter->asDatetime($bicycle->create_time, 'php:Y-m-d H:i:s'),
                    ],
                    [
                        'label' => $bicycle->advert->getAttributeLabel('rental_price'),
                        'value' => $bicycle->advert->rental_price . ' Ft',
                    ],
                    [
                        'label' => $bicycle->advert->getAttributeLabel('rental_city'),
                        'value' => $bicycle->advert->rental_city,
                    ],
                    [
                        'label' => $bicycle->advert->getAttributeLabel('rental_start_time'),
                        'value' => $bicycle->advert->rental_start_time,
                    ],
                    [
                        'label' => $bicycle->advert->getAttributeLabel('rental_end_time'),
                        'value' => $bicycle->advert->rental_end_time,
                    ],
                ],
                'options' => ['class' => 'table table-striped table-bordered detail-view m-0'],
            ]); ?>
        </div>
    </div>
</div>

<hr>

<div class="d-flex justify-content-center mb-4 mt-4">
    <h4>Megjegyzések</h4>
</div>

<div class="row justify-content-center border rounded mb-4">
    <?php if (!Yii::$app->user->isGuest) { ?>
        <?= $this->render('/comment/_create', ['commentForm' => $commentForm]) ?>
    <?php } ?>

    <?= $this->render('/comment/_view', ['advert' => $advert]) ?>
</div>

<hr>

<div class="d-flex justify-content-center mb-4 mt-4">
    <h4>Üzenj a hirdetés tulajdonosnak!</h4>
</div>

<div class="row justify-content-between border rounded">
    <div class="col-6 d-flex align-items-center justify-content-center mt-4 mb-4">
        <?php if (!Yii::$app->user->isGuest) { ?>
            <?= Html::a('Közvetlen email küldése', Url::to('mailto:' . $advert->user->email), ['class' => 'btn btn-primary']) ?>
        <?php } else { ?>
            <?= Html::tag('div', 'Közvetlen email küldése', ['class' => 'btn btn-secondary']) ?>
        <?php } ?>
    </div>

    <?= $this->render('/advertContact/_create', ['advertContactForm' => $advertContactForm]) ?>
</div>
