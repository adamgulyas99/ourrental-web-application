<?php
/** @var yii\web\View $this */
/** @var ActiveForm $form */
/** @var AdvertForm $advertForm */
/** @var BicycleForm $bicycleForm */
/** @var AdvertImageForm $advertImageForm */

use frontend\models\AdvertForm;
use frontend\models\AdvertImageForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \common\models\Bicycle;
use \frontend\models\BicycleForm;

$this->title = 'Bicikli hirdetés létrehozása';
?>

<?php $form = ActiveForm::begin(['id' => 'advert-form']); ?>

<div class="row">
    <div class="col-6">
        <?= $form->field($bicycleForm, 'name')->textInput() ?>
        <?= $form->field($bicycleForm, 'description')->textarea() ?>
        <?= $form->field($bicycleForm, 'brand')->textInput() ?>
        <?= $form->field($bicycleForm, 'type')->dropDownList(Bicycle::itemAlias('type'), ['prompt' => '']) ?>
        <?= $form->field($bicycleForm, 'gender')->dropDownList(Bicycle::itemAlias('gender')) ?>
        <?= $form->field($bicycleForm, 'speed')->dropDownList(Bicycle::itemAlias('speed')) ?>
        <?= $form->field($bicycleForm, 'wheelSize')->dropDownList(Bicycle::itemAlias('wheelSize')) ?>

        <hr>

        <?= $form->field($advertImageForm, 'imageFile')->fileInput() ?>
    </div>

    <div class="col-6">
        <?= $this->render('/advert/_createRentalData', ['advertForm' => $advertForm, 'form' => $form]) ?>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <hr>

        <div class="form-group text-center">
            <?= Html::submitButton('Létrehozás', ['class' => 'btn btn-primary', 'name' => 'advert-button']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
