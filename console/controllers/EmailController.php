<?php
/** @noinspection PhpUndefinedFieldInspection */
/** @noinspection PhpUndefinedMethodInspection */

namespace console\controllers;

use common\components\Email;
use \yii\console\Controller;

class EmailController extends Controller
{
    public function actionSendDummyEmail()
    {
        Email::sendEmail('Proba Email', 'adam.rackhost.test@gmail.com', null, null, null, 'Sima szöveg.');
    }
}