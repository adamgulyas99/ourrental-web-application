<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m200930_164056_addFreeTrialColumnToUserTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'free_trial', $this->boolean()->notNull()->defaultValue(true));

        $this->createIndex('idx_user_free_trial', 'user', 'free_trial');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'free_trial');
    }
}
