<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%advert}}`.
 */
class m200502_071228_createAdvertTable extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $this->createTable('{{%advert}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'model_id' => $this->integer()->notNull(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'type' => $this->smallInteger(),
            'data' => $this->json(),
            'views' => $this->bigInteger(),
            'deleted' => $this->boolean(),
            'create_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'update_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('idx_advert_model_id', 'advert', 'model_id');
        $this->createIndex('idx_advert_title', 'advert', 'title');
        $this->createIndex('idx_advert_type', 'advert', 'type');
        $this->createIndex('idx_advert_deleted', 'advert', 'deleted');
        $this->createIndex('idx_advert_views', 'advert', 'views');

        $this->addForeignKey('fk_advert_user_id', 'advert', 'user_id', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%advert}}');
    }
}
