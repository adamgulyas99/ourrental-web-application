<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bicycle}}`.
 */
class m200502_065819_createBicycleTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bicycle}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'brand' => $this->string(255)->notNull(),
            'type' => $this->smallInteger()->notNull(),
            // férfi vagy női vagy unisex(3)
            'gender' => $this->smallInteger()->notNull()->defaultValue(3),
            // sebesség (egy vagy több)
            'speed' => $this->smallInteger()->notNull(),
            'wheel_size' => $this->integer(3),
            'create_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'update_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('idx_bicycle_name', 'bicycle', 'name');
        $this->createIndex('idx_bicycle_brand', 'bicycle', 'brand');
        $this->createIndex('idx_bicycle_type', 'bicycle', 'type');
        $this->createIndex('idx_bicycle_gender', 'bicycle', 'gender');
        $this->createIndex('idx_bicycle_speed', 'bicycle', 'speed');
        $this->createIndex('idx_bicycle_wheel_size', 'bicycle', 'wheel_size');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bicycle}}');
    }
}
