<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_rating}}`.
 */
class m200910_164900_createUserRatingTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_rating}}', [
            'id' => $this->primaryKey(),
            'create_user_id' => $this->integer()->notNull(),
            'advert_id' => $this->integer()->notNull(),
            'rating' => $this->integer()->notNull(),
            'create_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('idx_user_rating_create_user_id', 'user_rating', 'create_user_id');
        $this->createIndex('idx_user_rating_advert_id', 'user_rating', 'advert_id');
        $this->createIndex('idx_user_rating_rating', 'user_rating', 'rating');

        $this->addForeignKey('fk_user_rating_create_user_id', 'user_rating', 'create_user_id', 'user', 'id');
        $this->addForeignKey('fk_user_rating_advert_id', 'user_rating', 'advert_id', 'advert', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_rating}}');
    }
}
