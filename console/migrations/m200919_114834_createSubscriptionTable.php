<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%subscription}}`.
 */
class m200919_114834_createSubscriptionTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%subscription}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'paid' => $this->boolean()->notNull(),
            'price' => $this->string(255)->notNull(),
            'start_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'end_time' => $this->timestamp()->notNull(),
            'create_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'update_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('idx_subscription_user_id', 'subscription', 'user_id');
        $this->createIndex('idx_subscription_name', 'subscription', 'name');
        $this->createIndex('idx_subscription_paid', 'subscription', 'paid');
        $this->createIndex('idx_subscription_price', 'subscription', 'price');
        $this->createIndex('idx_subscription_start_time', 'subscription', 'start_time');
        $this->createIndex('idx_subscription_end_time', 'subscription', 'end_time');

        $this->addForeignKey('fk_subscription_user_id', 'subscription', 'user_id', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%subscription}}');
    }
}
