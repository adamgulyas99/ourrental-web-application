<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%advert}}`.
 */
class m200919_121501_addSubIdColumnToAdvertTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%advert}}', 'sub_id', $this->integer());

        $this->createIndex('idx_advert_sub_id', 'advert', 'sub_id');

        $this->addForeignKey('fk_advert_sub_id', 'advert', 'sub_id', 'subscription', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_advert_sub_id', 'advert');

        $this->dropIndex('idx_advert_sub_id', 'advert');

        $this->dropColumn('{{%advert}}', 'sub_id');
    }
}
