<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%motorbike}}`.
 */
class m200914_172030_createMotorbikeTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%motorbike}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'brand' => $this->string(255)->notNull(),
            'type' => $this->smallInteger()->notNull(),
            'create_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'update_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('idx_motorbike_name', 'motorbike', 'name');
        $this->createIndex('idx_motorbike_brand', 'motorbike', 'brand');
        $this->createIndex('idx_motorbike_type', 'motorbike', 'type');
        $this->createIndex('idx_motorbike_create_time', 'motorbike', 'create_time');
        $this->createIndex('idx_motorbike_update_time', 'motorbike', 'update_time');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%motorbike}}');
    }
}
