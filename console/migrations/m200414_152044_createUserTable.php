<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m200414_152044_createUserTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull()->unique(),
            'password' => $this->string()->notNull(),
            //1 -> férfi, 2 -> nő
            'gender' => $this->smallInteger()->notNull()->defaultValue(1),
            'zip_code' => $this->integer(8),
            'country' => $this->string(3),
            'city' => $this->string(255),
            'phone' => $this->string(20),
            'status' => $this->smallInteger()->notNull(),
            'password_reset_token' => $this->string(255)->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'reg_time' => $this->timestamp()->notNull()->defaultValue(date('Y-m-d H:i:s')),
            'update_time' => $this->timestamp()->notNull()->defaultValue(date('Y-m-d H:i:s')),
            'verification_token' => $this->string(255)->defaultValue(null),
        ]);

        $this->createIndex('idx_user_email', 'user', 'email');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
