<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%car}}`.
 */
class m200927_115310_createCarTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'brand' => $this->string(255)->notNull(),
            'type' => $this->smallInteger()->notNull(),
            'age_group' => $this->integer(),
            'air_conditioner' => $this->boolean(),
            'create_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'update_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('idx_car_name', 'car', 'name');
        $this->createIndex('idx_car_brand', 'car', 'brand');
        $this->createIndex('idx_car_age_group', 'car', 'age_group');
        $this->createIndex('idx_car_air_conditioner', 'car', 'air_conditioner');
        $this->createIndex('idx_car_type', 'car', 'type');
        $this->createIndex('idx_car_create_time', 'car', 'create_time');
        $this->createIndex('idx_car_update_time', 'car', 'update_time');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%car}}');
    }
}
