<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%advert}}`.
 */
class m201004_144329_addRentalColumnsToAdvertTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('advert', 'rental_price', $this->string());
        $this->addColumn('advert', 'rental_city', $this->string(255)->notNull());
        $this->addColumn('advert', 'rental_start_time', $this->timestamp()->notNull());
        $this->addColumn('advert', 'rental_end_time', $this->timestamp()->notNull());

        $this->createIndex('idx_advert_rental_price', 'advert', 'rental_price');
        $this->createIndex('idx_advert_rental_city', 'advert', 'rental_city');
        $this->createIndex('idx_advert_rental_start_time', 'advert', 'rental_start_time');
        $this->createIndex('idx_advert_rental_end_time', 'advert', 'rental_end_time');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('advert', 'rental_price');
        $this->dropColumn('advert', 'rental_city');
        $this->dropColumn('advert', 'rental_start_time');
        $this->dropColumn('advert', 'rental_end_time');
    }
}
