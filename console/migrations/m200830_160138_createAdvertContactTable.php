<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%advert_contact}}`.
 */
class m200830_160138_createAdvertContactTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advert_contact}}', [
            'id' => $this->primaryKey(),
            'advert_id' => $this->integer()->notNull(),
            'name' => $this->string(50)->notNull(),
            'email' => $this->string(255)->notNull(),
            'message' => $this->text()->notNull(),
            'create_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('idx_advert_contact_advert_id', 'advert_contact', 'advert_id');
        $this->createIndex('idx_advert_contact_name', 'advert_contact', 'name');
        $this->createIndex('idx_advert_contact_email', 'advert_contact', 'email');

        $this->addForeignKey('fk_advert_contact_advert_id', 'advert_contact', 'advert_id', 'advert', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%advert_contact}}');
    }
}
