<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comment}}`.
 */
class m200829_153950_createCommentTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'advert_id' => $this->integer()->notNull(),
            'subject' => $this->string(255)->notNull(),
            'content' => $this->text()->notNull(),
        ]);

        $this->createIndex('idx_comment_user_id', 'comment', 'user_id');
        $this->createIndex('idx_comment_advert_id', 'comment', 'advert_id');

        $this->addForeignKey('fk_comment_user_id', 'comment', 'user_id', 'user', 'id');
        $this->addForeignKey('fk_comment_advert_id', 'comment', 'advert_id', 'advert', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%comment}}');
    }
}
