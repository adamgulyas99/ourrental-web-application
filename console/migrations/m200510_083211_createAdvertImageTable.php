<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%advert_image}}`.
 */
class m200510_083211_createAdvertImageTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advert_image}}', [
            'id' => $this->primaryKey(),
            'advert_id' => $this->integer()->notNull(),
            'name' => $this->string(255),
            'type' => $this->smallInteger()->notNull(),
            'upload_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('idx_advert_image_advert_id', 'advert_image', 'advert_id');
        $this->createIndex('idx_advert_image_name', 'advert_image', 'name');
        $this->createIndex('idx_advert_image_type', 'advert_image', 'type');

        $this->addForeignKey('fk_advert_image_advert_id', 'advert_image', 'advert_id', 'advert', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%advert_image}}');
    }
}
