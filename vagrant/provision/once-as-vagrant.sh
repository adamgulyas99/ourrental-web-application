#!/usr/bin/env bash

source /app/vagrant/provision/common.sh

#== Import script args ==

github_token=$(echo "$1")

#== Provision script ==

info "Provision-script user: `whoami`"

info "Configure composer"
composer config --global github-oauth.github.com ${github_token}
echo "Done!"

info "Install project dependencies"
cd /app
composer --no-progress --prefer-dist install

info "Init project"
./init --env=Development --overwrite=y

info "Apply migrations"
./yii migrate <<< "yes"
./yii_test migrate <<< "yes"

info "Create bash-alias 'app' for vagrant user"
echo 'alias app="cd /app"' | tee /home/vagrant/.bash_aliases

info "Enabling colorized prompt for guest console"
sed -i "s/#force_color_prompt=yes/force_color_prompt=yes/" /home/vagrant/.bashrc

info "Create common local config files"
if [ -f /app/common/config/main-local.php ]; then
  rm /app/common/config/main-local.php
  cp /app/common/config/main-local.tpl.php /app/common/config/main-local.php
fi

if [ -f /app/common/config/params-local.php ]; then
  rm /app/common/config/params-local.php
  cp /app/common/config/params-local.tpl.php /app/common/config/params-local.php
fi

if [ -f /app/common/config/test-local.php ]; then
  rm /app/common/config/test-local.php
  cp /app/common/config/test-local.tpl.php /app/common/config/test-local.php
fi

info "Create backend local config files"
if [ -f /app/backend/config/main-local.php ]; then
  rm /app/backend/config/main-local.php
  cp /app/backend/config/main-local.tpl.php /app/backend/config/main-local.php
fi

if [ -f /app/backend/config/params-local.php ]; then
  rm /app/backend/config/params-local.php
  cp /app/backend/config/params-local.tpl.php /app/backend/config/params-local.php
fi

if [ -f /app/backend/config/test-local.php ]; then
  rm /app/backend/config/test-local.php
  cp /app/backend/config/test-local.tpl.php /app/backend/config/test-local.php
fi

info "Create frontend local config files"
if [ -f /app/frontend/config/main-local.php ]; then
  rm /app/frontend/config/main-local.php
  cp /app/frontend/config/main-local.tpl.php /app/frontend/config/main-local.php
fi

if [ -f /app/frontend/config/params-local.php ]; then
  rm /app/frontend/config/params-local.php
  cp /app/frontend/config/params-local.tpl.php /app/frontend/config/params-local.php
fi

if [ -f /app/frontend/config/test-local.php ]; then
  rm /app/frontend/config/test-local.php
  cp /app/frontend/config/test-local.tpl.php /app/frontend/config/test-local.php
fi
