#!/usr/bin/env bash

#### Import script args ####

db_user_name=$(echo "$1")
db_user_pass=$(echo "$2")
db_name=$(echo "$3")
db_data=$(echo "$4")

#### Bash helpers ####

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#### Provision script ####

info "Initialize database: $db_name"
echo "CREATE USER $db_user_name WITH PASSWORD '$db_user_pass';" | psql -U postgres
echo "CREATE DATABASE $db_name OWNER $db_user_name ENCODING 'UTF8' LC_COLLATE = 'hu_HU.UTF-8' LC_CTYPE = 'hu_HU.UTF-8' TEMPLATE = template0;" | psql -U postgres
echo "CREATE DATABASE ${db_name}_test OWNER $db_user_name ENCODING 'UTF8' LC_COLLATE = 'hu_HU.UTF-8' LC_CTYPE = 'hu_HU.UTF-8' TEMPLATE = template0;" | psql -U postgres

if [ -f $db_data ]; then
    psql -U $db_user_name $db_name < $db_data
fi
