<div>
    <p>
        Köszönjük szépen a regisztrációját!
        <br>
        <br>
        Ezentúl hozzáfér az oldalunk által biztosított hirdetés létrehozás funkcióhoz, amely segítségével
        bátran hasznossá teheti saját szabaddá vált járművét.
        <br>
        A hirdetés kereső oldalon pedig kedvére választhat a járművek széles választékából.
    </p>
</div>