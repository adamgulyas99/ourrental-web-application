<?php
/** @var View $this */
/** @var string $addressee */
/** @var string $content */
/** @var MessageInterface $message */

use yii\helpers\Html;
use yii\mail\MessageInterface;
use yii\web\View;

$headerParams = [];
if (!empty($addressee)) {
    $headerParams['addressee'] = $addressee;
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>

    <?= $this->render('_header', $headerParams) ?>
    <?= $content ?>
    <?= $this->render('_footer') ?>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
