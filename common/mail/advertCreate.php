<div>
    <p>
        Sikeres hirdetés létrehozás!
        <br>
        <br>
        Hirdetésére várhatóan rövid időn belül érdeklődők fognak írni, amelyet szintén email formájában fog megkapni.
        Kérjük ügyeljen beérkező leveleire, illetve, ha ott nem találja a várt email-t,
        akkor kérjük ellenőrizze spam mappájának tartalmát.
        <br>
        <br>
        Egyéb kérdés esetén írjon nekünk a <?= Yii::$app->params['adminEmail'] ?> címre.
    </p>
</div>