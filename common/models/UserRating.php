<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * UserRating model
 *
 * @property int $create_user_id
 * @property int $advert_id
 * @property int $rating
 * @property string $create_time
 */
class UserRating extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%user_rating}}';
    }

    public function rules(): array
    {
        return [
            ['create_time', 'default', 'value' => new Expression('NOW()'), 'on' => 'insert'],

            [['create_user_id', 'advert_id', 'rating'], 'required'],
            [['create_user_id', 'advert_id', 'rating'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'create_user_id' => Yii::t('app', 'Létrehozó felhasználó ID'),
            'advert_id' => Yii::t('app', 'Értékelt hirdetés ID'),
            'rating' => Yii::t('app', 'Értékelés'),
        ];
    }

    /**
     * @param int $createUserId
     * @param int $advertId
     * @return UserRating|null
     */
    public static function findByCreateUserIdAndAdvertId(int $createUserId, int $advertId)
    {
        return static::findOne(['create_user_id' => $createUserId, 'advert_id' => $advertId]);
    }
}