<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Comment form
 */
class CommentForm extends Model
{
    /** @var int */
    public $advertId;
    /** @var string */
    public $content;
    /** @var string */
    public $subject;
    /** @var int */
    public $userId;

    public function rules(): array
    {
        return [
            ['subject', 'string', 'max' => 255],
            ['content', 'string'],

            [['userId', 'advertId'], 'number'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'advertId' => Yii::t('app', 'Hirdetés ID'),
            'content' => Yii::t('app', 'Üzenet'),
            'subject' => Yii::t('app', 'Tárgy'),
            'userId' => Yii::t('app', 'Felhasználó ID'),
        ];
    }

    public function fillTo(Comment $comment, int $advertId): Comment
    {
        $comment->advert_id = $advertId;
        $comment->content = $this->content;
        $comment->subject = $this->subject;
        $comment->user_id = Yii::$app->user->id;

        return $comment;
    }
}