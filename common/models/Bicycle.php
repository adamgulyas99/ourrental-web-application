<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Bicycle Model
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $brand
 * @property int $type
 * @property int $gender
 * @property int $speed
 * @property int $wheel_size
 * @property string $create_time
 * @property string $update_time
 *
 * @property Advert $advert
 */
class Bicycle extends ActiveRecord
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_UNISEX = 3;

    const SPEED_SINGLE = 1;
    const SPEED_MULTIPLE = 2;

    const TYPE_URBAN = 1;
    const TYPE_MOUNTAIN = 2;

    const WHEEL_SIZE_18 = 18;
    const WHEEL_SIZE_19 = 19;
    const WHEEL_SIZE_20 = 20;
    const WHEEL_SIZE_21 = 21;
    const WHEEL_SIZE_22 = 22;

    public static function tableName(): string
    {
        return '{{%bicycle}}';
    }

    public function rules(): array
    {
        return [
            [['name', 'description', 'brand', 'type', 'gender', 'speed'], 'required'],
            [['name', 'brand'], 'trim'],
            [['name', 'brand'], 'string', 'max' => 255],
            ['description', 'string'],
            [['type', 'gender', 'speed', 'wheel_size'], 'integer'],
            ['gender', 'default', 'value' => static::GENDER_UNISEX],

            ['create_time', 'default', 'value' => new Expression('NOW()'), 'on' => 'insert'],
            ['update_time', 'default', 'value' => new Expression('NOW()'), 'on' => 'update'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('app', 'Cím'),
            'brand' => Yii::t('app', 'Márka'),
            'create_time' => Yii::t('app', 'Létrehozás dátuma'),
            'description' => Yii::t('app', 'Leírás'),
            'gender' => Yii::t('app', 'Nem'),
            'speed' => Yii::t('app', 'Sebesség típus'),
            'type' => Yii::t('app', 'Típus'),
            'wheel_size' => Yii::t('app', 'Kerék méret'),
        ];
    }

    public function getAdvert()
    {
        return $this->hasOne(Advert::class, ['model_id' => 'id']);
    }

    public function constructAdditionalData(): array
    {
        return [
            'brand' => $this->brand,
            'type' => $this->type,
            'gender' => $this->gender,
            'speed' => $this->speed,
            'wheelSize' => $this->wheel_size,
        ];
    }

    public static function itemAlias(string $col, $value = null)
    {
        $result = [
            'gender' => [
                Bicycle::GENDER_MALE => 'Férfi',
                Bicycle::GENDER_FEMALE => 'Nő',
                Bicycle::GENDER_UNISEX => 'Uniszex',
            ],
            'speed' => [
                Bicycle::SPEED_SINGLE => 'Egy sebességes',
                Bicycle::SPEED_MULTIPLE => 'Több sebességes',
            ],
            'type' => [
                Bicycle::TYPE_URBAN => 'Városi',
                Bicycle::TYPE_MOUNTAIN => 'Hegyi',
            ],
            'wheelSize' => [
                Bicycle::WHEEL_SIZE_18 => '18"',
                Bicycle::WHEEL_SIZE_19 => '19"',
                Bicycle::WHEEL_SIZE_20 => '20"',
                Bicycle::WHEEL_SIZE_21 => '21"',
                Bicycle::WHEEL_SIZE_22 => '22"',
            ],
        ];

        if (isset($value)) {
            return isset($result[$col][$value]) ? $result[$col][$value] : false;
        }

        return isset($result[$col]) ? $result[$col] : false;
    }

    public static function findById(int $id)
    {
        return static::findOne(['id' => $id]);
    }
}