<?php
namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Subscription model
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property bool $paid
 * @property string $price
 * @property string $start_time
 * @property string $end_time
 * @property string $create_time
 * @property string $update_time
 *
 * @property Advert[] $adverts
 * @property User $user
 */
class Subscription extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%subscription}}';
    }

    public function rules(): array
    {
        return [
            ['paid', 'boolean'],
            ['user_id', 'integer'],

            [
                'user_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id']
            ],

            [['user_id', 'name', 'paid', 'price', 'end_time'], 'required'],
            [['name', 'price'], 'string', 'max' => 255],
            [['start_time', 'end_time', 'create_time', 'update_time'], 'safe'],

            ['create_time', 'default', 'value' => new Expression('NOW()'), 'on' => 'insert'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'user_id' => Yii::t('app', 'Felhasználó ID'),
            'name' => Yii::t('app', 'Elnevezés'),
            'paid' => Yii::t('app', 'Fizetett'),
            'price' => Yii::t('app', 'Ár'),
            'start_time' => Yii::t('app', 'Kezdete'),
            'end_time' => Yii::t('app', 'Vége'),
            'create_time' => Yii::t('app', 'Létrehozás dátuma'),
            'update_time' => Yii::t('app', 'Módosítás dátuma'),
        ];
    }

    public function getAdverts(): ActiveQuery
    {
        return $this->hasMany(Advert::class, ['sub_id' => 'id']);
    }

    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public static function findById(int $id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUserId(int $userId)
    {
        return static::findOne(['user_id' => $userId]);
    }
}
