<?php
namespace common\models;

use common\components\EventHandler;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Advert model
 *
 * @property int $id
 * @property int $user_id
 * @property int $model_id
 * @property string $title
 * @property string $description
 * @property int $type
 * @property string $data
 * @property bool $deleted
 * @property int $views
 * @property string $create_time
 * @property string $update_time
 * @property string $rental_price
 * @property string $rental_city
 * @property string $rental_start_time
 * @property string $rental_end_time
 *
 * @property AdvertImage[] $advertImages
 * @property AdvertImage $advertCoverImage
 * @property Comment[] $comments
 * @property User $user
 */
class Advert extends ActiveRecord
{
    const EVENT_AFTER_CREATE = 'onAfterCreate';

    const TYPE_BICYCLE = 1;
    const TYPE_MOTORBIKE = 2;
    const TYPE_CAR = 3;

    public static function tableName(): string
    {
        return '{{%advert}}';
    }

    public function rules(): array
    {
        return [
            ['type', 'integer', 'max' => 3],
            ['deleted', 'boolean'],
            ['deleted', 'default', 'value' => false],

            [['user_id', 'model_id', 'rental_city', 'rental_start_time', 'rental_end_time'], 'required'],
            [['user_id', 'model_id', 'views'], 'integer'],
            [['description', 'data', 'rental_price'], 'string'],
            [['rental_city', 'title'], 'string', 'max' => 255],

            ['create_time', 'default', 'value' => new Expression('NOW()'), 'on' => 'insert'],
            ['update_time', 'default', 'value' => new Expression('NOW()'), 'on' => 'update'],

            [['rental_start_time', 'rental_end_time'], 'safe'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'title' => Yii::t('app', 'Cím'),
            'description' => Yii::t('app', 'Leírás'),
            'type' => Yii::t('app', 'Típus'),
            'data' => Yii::t('app', 'Adat'),
            'deleted' => Yii::t('app', 'Törölt'),
            'rental_price' => Yii::t('app', 'Bérlés ára'),
            'rental_city' => Yii::t('app', 'Bérlés helyszíne'),
            'rental_start_time' => Yii::t('app', 'Bérlés kezdete'),
            'rental_end_time' => Yii::t('app', 'Bérlés vége'),
        ];
    }

    public function init()
    {
        $this->on(self::EVENT_AFTER_CREATE, [EventHandler::class, 'sendAdvertCreationEmail']);

        parent::init();
    }

    public static function itemAlias(string $col, $value = null)
    {
        $result = [
            'type' => [
                self::TYPE_BICYCLE => 'Bicikli',
                self::TYPE_MOTORBIKE => 'Motor',
                self::TYPE_CAR => 'Autó',
            ],
        ];

        if (isset($value)) {
            return isset($result[$col][$value]) ? $result[$col][$value] : false;
        }

        return isset($result[$col]) ? $result[$col] : false;
    }

    public static function search(ActiveQuery $query, $params = []): ActiveQuery
    {
        if (isset($params['owned'])) {
            $query->where(['user_id' => Yii::$app->user->id]);
        }

        if (isset($params['title'])) {
            $query->where("title LIKE '%{$params['title']}%'");
        }

        if (isset($params['description'])) {
            $query->where("title LIKE '%{$params['description']}%'");
        }

        if (isset($params['type'])) {
            $query->where(['type' => $params['type']]);
        }

        if (isset($params['rentalPrice'])) {
            $query->where(['rental_price' => $params['rentalPrice']]);
        }

        if (isset($params['rentalCity'])) {
            $query->where("rental_city LIKE '%{$params['rentalCity']}%'");
        }

        return $query;
    }

    //Relations

    public function getAdvertImages(): ActiveQuery
    {
        return $this->hasMany(AdvertImage::class, ['advert_id' => 'id']);
    }

    public function getAdvertCoverImage(): ActiveQuery
    {
        return $this->hasOne(AdvertImage::class, ['advert_id' => 'id'])
            ->where('type = :type', [':type' => AdvertImage::TYPE_COVER]);
    }

    public function getComments(): ActiveQuery
    {
        return $this->hasMany(Comment::class, ['advert_id' => 'id']);
    }

    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    //Functions

    public function setDeleted()
    {
        $this->deleted = true;
    }

    public function addView()
    {
        $this->views = $this->views + 1;
    }

    //Queries

    public static function findById(int $id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function getActiveAdverts(): ActiveQuery
    {
        return static::find()->where(['deleted' => false])->orderBy('create_time DESC');
    }
}