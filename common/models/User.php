<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\components\EventHandler;

/**
 * User model
 *
 * @property string $auth_key
 * @property string $city
 * @property string $country
 * @property string $email
 * @property int $gender
 * @property int $id
 * @property string $password
 * @property string $password_reset_token
 * @property string $phone
 * @property string $reg_time
 * @property int $status
 * @property string $update_time
 * @property int $zip_code
 * @property string $verification_token
 * @property bool $free_trial
 * @property bool $is_admin
 *
 * @property Advert[] $adverts
 * @property Subscription[] $subscriptions
 */
class User extends ActiveRecord implements IdentityInterface
{
    const EVENT_AFTER_REGISTER = 'onAfterRegister';

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const IS_ADMIN_TRUE = true;
    const IS_ADMIN_FALSE = false;

    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['auth_key', 'string', 'max' => 32],

            ['email', 'email'],

            ['gender', 'integer'],
            ['gender', 'filter', 'filter' => 'intval', 'skipOnEmpty' => false],
            ['gender', 'in', 'range' => [1, 2]],

            ['is_admin', 'default', 'value' => self::IS_ADMIN_FALSE],

            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

            ['phone', 'string', 'max' => 20],

            [['auth_key', 'email', 'gender', 'password', 'status'], 'required'],
            [['city', 'email', 'password_reset_token', 'verification_token'], 'string', 'max' => 255],

            [['free_trial', 'zip_code', 'reg_time', 'update_time'], 'safe'],
        ];
    }

    public function init()
    {
        $this->on(self::EVENT_AFTER_REGISTER, [EventHandler::class, 'sendRegistrationEmail']);

        parent::init();
    }

    public function getAdverts(): ActiveQuery
    {
        return $this->hasMany(Advert::class, ['user_id' => 'id']);
    }

    public function getSubscriptions(): ActiveQuery
    {
        return $this->hasMany(Subscription::class, ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public static function findByEmail(string $email)
    {
        return static::findOne(['email' => $email]);
    }

    public static function findById(int $id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return void|IdentityInterface|null
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne(['password_reset_token' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token)
    {
        return static::findOne(['verification_token' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @throws Exception
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @param $password
     * @throws Exception
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
}
