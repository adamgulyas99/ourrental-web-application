<?php
namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * AdvertImage Model
 *
 * @property int $id
 * @property int $advert_id
 * @property string $name
 * @property int $type
 * @property string $upload_time
 *
 * @property Advert $advert
 */
class AdvertImage extends ActiveRecord
{
    const TYPE_COVER = 1;
    const TYPE_GALLERY = 2;

    public static function tableName(): string
    {
        return '{{%advert_image}}';
    }

    public function rules(): array
    {
        return [
            [['advert_id', 'type'], 'required'],
            ['advert_id', 'integer'],
            ['name', 'string', 'max' => 255],

            ['upload_time', 'default', 'value' => new Expression('NOW()'), 'on' => 'insert'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'advert_id' => Yii::t('app', 'Hirdetés ID'),
            'name' => Yii::t('app', 'File név'),
            'type' => Yii::t('app', 'Típus'),
            'upload_time' => Yii::t('app', 'Feltöltés időpontja'),
        ];
    }

    public function getAdvert(): ActiveQuery
    {
        return $this->hasOne(Advert::class, ['id' => 'advert_id']);
    }

    public static function findByAdvertId(int $advertId)
    {
        return self::findOne(['advert_id' => $advertId]);
    }
}