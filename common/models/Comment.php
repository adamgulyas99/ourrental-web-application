<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Comment model
 *
 * @property int $id
 * @property int $user_id
 * @property int $advert_id
 * @property string $subject
 * @property string $content
 *
 * @property Advert $advert
 * @property User $user
 */
class Comment extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%comment}}';
    }

    public function rules(): array
    {
        return [
            ['subject', 'string', 'max' => 255],
            ['content', 'string'],

            [['user_id', 'advert_id'], 'integer'],
            [['user_id', 'advert_id', 'subject', 'content'], 'required'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'subject' => Yii::t('app', 'Tárgy'),
            'content' => Yii::t('app', 'Üzenet'),
        ];
    }

    public function getAdvert()
    {
        return $this->hasOne(Advert::class, ['id' => 'advert_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public static function findById(int $id): Comment
    {
        return static::findOne(['id' => $id]);
    }
}