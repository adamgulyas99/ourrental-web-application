<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm Model
 */
class LoginForm extends Model
{
    /** @var string */
    public $email;
    /** @var string */
    public $password;
    /** @var bool  */
    public $rememberMe = true;
    /** @var User */
    private $user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required', 'message' => '{attribute} kitöltése kötelező.'],
            ['email', 'string', 'max' => 255],
            ['password', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('app', 'Hibás email cím és jelszó páros.'));
            }
        }
    }

    public function attributeLabels(): array
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Jelszó'),
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        
        return false;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        $this->user = User::findByEmail($this->email);
        if ($this->user === null) {
            return null;
        }

        return $this->user;
    }
}
