<?php
/** @noinspection SpellCheckingInspection */

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Motorbike model
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $brand
 * @property int $type
 * @property string $create_time
 * @property string $update_time
 *
 * @property Advert $advert
 */
class Motorbike extends ActiveRecord
{
    const TYPE_CHOPPER = 1;
    const TYPE_CLASSIC = 2;
    const TYPE_ENDURO = 3;
    const TYPE_MOPED = 4;
    const TYPE_RACE = 5;
    const TYPE_SCOOTER = 6;
    const TYPE_SMALL_MOTOR = 7;
    const TYPE_TOUR = 8;
    const TYPE_QUAD = 9;

    public static function tableName(): string
    {
        return '{{%motorbike}}';
    }

    public function rules()
    {
        return [
            [['name', 'description', 'brand', 'type'], 'required'],
            [['name', 'brand'], 'trim'],
            [['name', 'brand'], 'string', 'max' => 255],
            ['description', 'string'],
            ['type', 'integer'],

            ['create_time', 'default', 'value' => new Expression('NOW()'), 'on' => 'insert'],
            ['update_time', 'default', 'value' => new Expression('NOW()'), 'on' => 'update'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('app', 'Cím'),
            'brand' => Yii::t('app', 'Márka'),
            'create_time' => Yii::t('app', 'Létrehozás dátuma'),
            'description' => Yii::t('app', 'Leírás'),
            'type' => Yii::t('app', 'Típus'),
        ];
    }

    public function getAdvert()
    {
        return $this->hasOne(Advert::class, ['model_id' => 'id']);
    }

    public function constructAdditionalData(): array
    {
        return [
            'brand' => $this->brand,
            'type' => $this->type,
        ];
    }

    public static function itemAlias(string $col, $value = null)
    {
        $result = [
            'type' => [
                Motorbike::TYPE_CHOPPER => 'Chopper',
                Motorbike::TYPE_CLASSIC => 'Klasszikus',
                Motorbike::TYPE_ENDURO => 'Enduro',
                Motorbike::TYPE_MOPED => 'Moped',
                Motorbike::TYPE_RACE => 'Verseny',
                Motorbike::TYPE_SCOOTER => 'Robogó',
                Motorbike::TYPE_SMALL_MOTOR => 'Kismotor',
                Motorbike::TYPE_TOUR => 'Túra',
                Motorbike::TYPE_QUAD => 'Quad',
            ],
        ];

        if (isset($value)) {
            return isset($result[$col][$value]) ? $result[$col][$value] : false;
        }

        return isset($result[$col]) ? $result[$col] : false;
    }

    public static function findById(int $id)
    {
        return static::findOne(['id' => $id]);
    }
}