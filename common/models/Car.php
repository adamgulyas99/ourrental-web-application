<?php
/** @noinspection DuplicatedCode */

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%car}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $brand
 * @property int $type
 * @property int $age_group
 * @property bool $air_conditioner
 * @property string $create_time
 * @property string $update_time
 *
 * @property Advert $advert
 */
class Car extends ActiveRecord
{
    const AIR_CONDITIONER_YES = 1;
    const AIR_CONDITIONER_NO = 0;

    const TYPE_CABRIO = 1;
    const TYPE_CARAVAN = 2;
    const TYPE_CROSSOVER = 3;
    const TYPE_FAMILY = 4;
    const TYPE_PICKUP = 5;
    const TYPE_SPORT = 6;
    const TYPE_SUV = 7;

    public static function tableName()
    {
        return '{{%car}}';
    }

    public function rules(): array
    {
        return [
            ['description', 'string'],
            [['age_group', 'type'], 'integer'],
            ['type', 'default', 'value' => null],
            ['air_conditioner', 'boolean'],

            [['name', 'description', 'brand', 'type'], 'required'],
            [['name', 'brand'], 'string', 'max' => 255],

            [['create_time', 'update_time'], 'safe'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('app', 'Cím'),
            'brand' => Yii::t('app', 'Márka'),
            'create_time' => Yii::t('app', 'Létrehozás dátuma'),
            'description' => Yii::t('app', 'Leírás'),
            'type' => Yii::t('app', 'Típus'),
            'age_group' => Yii::t('app', 'Évjárat'),
            'air_conditioner' => Yii::t('app', 'Légkondicionáló'),
        ];
    }

    public function getAdvert()
    {
        return $this->hasOne(Advert::class, ['model_id' => 'id']);
    }

    public function constructAdditionalData(): array
    {
        return [
            'age_group' => $this->age_group,
            'air_conditioner' => $this->air_conditioner,
            'brand' => $this->brand,
            'type' => $this->type,
        ];
    }

    public static function itemAlias(string $col, $value = null)
    {
        $result = [
            'air_conditioner' => [
                self::AIR_CONDITIONER_YES => 'Van',
                self::AIR_CONDITIONER_NO => 'Nincs',
            ],
            'type' => [
                self::TYPE_CABRIO => 'Kabrió',
                self::TYPE_CARAVAN => 'Karaván',
                self::TYPE_CROSSOVER => 'Crossover',
                self::TYPE_FAMILY => 'Családi',
                self::TYPE_PICKUP => 'Pickup',
                self::TYPE_SPORT => 'Sportos',
                self::TYPE_SUV => 'Suv',
            ],
        ];

        if (isset($value)) {
            return isset($result[$col][$value]) ? $result[$col][$value] : false;
        }

        return isset($result[$col]) ? $result[$col] : false;
    }

    public static function findById(int $id)
    {
        return static::findOne(['id' => $id]);
    }
}
