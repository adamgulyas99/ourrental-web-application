<?php
return array_merge(require __DIR__ . '/test.php', [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=127.0.0.1;dbname=ourrental',
            'username' => 'ourrental',
            'password' => 'password',
            'charset' => 'utf8',
        ],
    ],
]);