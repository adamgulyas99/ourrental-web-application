<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Motorbike;
use common\tests\UnitTester;
use yii\db\ActiveQuery;

class MotorbikeTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $motorbike = new Motorbike();

        $motorbike->name = "Új bicikli";
        $this->assertTrue($motorbike->validate('name'));

        $motorbike->name = null;
        $this->assertFalse($motorbike->validate('name'));

        $motorbike->description = "Leírás";
        $this->assertTrue($motorbike->validate('description'));

        $motorbike->description = null;
        $this->assertFalse($motorbike->validate('description'));

        $motorbike->brand = "Capriolo";
        $this->assertTrue($motorbike->validate('brand'));

        $motorbike->brand = null;
        $this->assertFalse($motorbike->validate('brand'));

        $motorbike->type = Motorbike::TYPE_ENDURO;
        $this->assertTrue($motorbike->validate('type'));

        $motorbike->type = null;
        $this->assertFalse($motorbike->validate('type'));

        $motorbike->create_time = date('Y-m-d H:i:s');
        $this->assertTrue($motorbike->validate('create_time'));

        $motorbike->update_time = date('Y-m-d H:i:s');
        $this->assertTrue($motorbike->validate('update_time'));
    }

    public function testRelations()
    {
        $motorbike = new Motorbike();

        $this->assertInstanceOf(ActiveQuery::class, $motorbike->getAdvert());
    }

    public function testConstructAdditionalData()
    {
        /** @var Motorbike $motorbike */
        $motorbike = Motorbike::find()->orderBy('id DESC')->one();

        $this->assertIsArray($motorbike->constructAdditionalData());
    }

    public function testItemAlias()
    {
        $this->assertFalse(Motorbike::itemAlias('randomCol'));

        $this->assertIsArray(Motorbike::itemAlias('type'));
        $this->assertIsString(Motorbike::itemAlias('type', Motorbike::TYPE_ENDURO));
    }

    public function testFindById()
    {
        /** @var Motorbike $motorbike */
        $motorbike = Motorbike::find()->orderBy('create_time DESC')->one();

        $this->assertInstanceOf(Motorbike::class, Motorbike::findById($motorbike->id));
    }
}
