<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Advert;
use common\models\Bicycle;
use common\tests\UnitTester;
use yii\db\ActiveQuery;

class BicycleTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $bicycle = new Bicycle();

        $bicycle->name = "Új bicikli";
        $this->assertTrue($bicycle->validate('name'));

        $bicycle->name = null;
        $this->assertFalse($bicycle->validate('name'));

        $bicycle->description = "Leírás";
        $this->assertTrue($bicycle->validate('description'));

        $bicycle->description = null;
        $this->assertFalse($bicycle->validate('description'));

        $bicycle->brand = "Capriolo";
        $this->assertTrue($bicycle->validate('brand'));

        $bicycle->brand = null;
        $this->assertFalse($bicycle->validate('brand'));

        $bicycle->type = Bicycle::TYPE_URBAN;
        $this->assertTrue($bicycle->validate('type'));

        $bicycle->type = null;
        $this->assertFalse($bicycle->validate('type'));

        $bicycle->gender = Bicycle::GENDER_UNISEX;
        $this->assertTrue($bicycle->validate('gender'));

        $bicycle->gender = null;
        $this->assertFalse($bicycle->validate('gender'));

        $bicycle->speed = Bicycle::SPEED_MULTIPLE;
        $this->assertTrue($bicycle->validate('speed'));

        $bicycle->speed = null;
        $this->assertFalse($bicycle->validate('speed'));

        $bicycle->wheel_size = Bicycle::WHEEL_SIZE_19;
        $this->assertTrue($bicycle->validate('wheel_size'));

        $bicycle->wheel_size = 'random szöveg';
        $this->assertFalse($bicycle->validate('wheel_size'));

        $bicycle->create_time = date('Y-m-d H:i:s');
        $this->assertTrue($bicycle->validate('create_time'));

        $bicycle->update_time = date('Y-m-d H:i:s');
        $this->assertTrue($bicycle->validate('update_time'));
    }

    public function testRelations()
    {
        $bicycle = new Bicycle();

        $this->assertInstanceOf(ActiveQuery::class, $bicycle->getAdvert());
    }

    public function testConstructAdditionalData()
    {
        /** @var Bicycle $bicycle */
        $bicycle = Bicycle::find()->orderBy('id DESC')->one();

        $this->assertIsArray($bicycle->constructAdditionalData());
    }

    public function testItemAlias()
    {
        $this->assertFalse(Bicycle::itemAlias('randomCol'));

        $this->assertIsArray(Bicycle::itemAlias('gender'));
        $this->assertIsString(Bicycle::itemAlias('gender', Bicycle::GENDER_UNISEX));

        $this->assertIsArray(Bicycle::itemAlias('speed'));
        $this->assertIsString(Bicycle::itemAlias('speed', Bicycle::SPEED_MULTIPLE));

        $this->assertIsArray(Bicycle::itemAlias('type'));
        $this->assertIsString(Bicycle::itemAlias('type', Bicycle::TYPE_URBAN));

        $this->assertIsArray(Bicycle::itemAlias('wheelSize'));
        $this->assertIsString(Bicycle::itemAlias('wheelSize', Bicycle::WHEEL_SIZE_19));
    }

    public function testFindById()
    {
        /** @var Bicycle $bicycle */
        $bicycle = Bicycle::find()->orderBy('create_time DESC')->one();

        $this->assertInstanceOf(Bicycle::class, Bicycle::findById($bicycle->id));
    }
}
