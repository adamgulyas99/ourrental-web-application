<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Comment;
use common\models\CommentForm;
use common\tests\UnitTester;

class CommentFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $commentForm = new CommentForm();

        $commentForm->advertId = 1;
        $this->assertTrue($commentForm->validate('advertId'));

        $commentForm->advertId = 'fdsgdfghdfg';
        $this->assertFalse($commentForm->validate('advertId'));

        $commentForm->userId = 1;
        $this->assertTrue($commentForm->validate('userId'));

        $commentForm->userId = 'fdsgdfg';
        $this->assertFalse($commentForm->validate('userId'));

        $commentForm->content = 'sdfsdffsd';
        $this->assertTrue($commentForm->validate('content'));

        $commentForm->content = 12332;
        $this->assertFalse($commentForm->validate('content'));

        $commentForm->subject = 'sdfsdffsd';
        $this->assertTrue($commentForm->validate('subject'));

        $commentForm->subject = 12122;
        $this->assertFalse($commentForm->validate('subject'));
    }

    public function testFillTo()
    {
        $commentForm = new CommentForm();
        $commentForm->setAttributes(['content' => 'dfgsdgg', 'subject' => 'fsgdfdg']);

        $comment = new Comment();
        $advertId = 1;

        $this->assertInstanceOf(Comment::class, $commentForm->fillTo($comment, $advertId));
    }
}