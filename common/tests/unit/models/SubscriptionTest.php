<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Subscription;
use common\models\User;
use common\tests\UnitTester;
use yii\db\ActiveQuery;

class SubscriptionTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $subscription = new Subscription();

        $subscription->user_id = 1;
        $this->assertTrue($subscription->validate('user_id'));

        $subscription->user_id = 'fdsgdfghdfg';
        $this->assertFalse($subscription->validate('user_id'));

        $subscription->name = 'sfdsdfsdfsdf';
        $this->assertTrue($subscription->validate('name'));

        $subscription->name = null;
        $this->assertFalse($subscription->validate('name'));

        $subscription->paid = true;
        $this->assertTrue($subscription->validate('paid'));

        $subscription->paid = 12332;
        $this->assertFalse($subscription->validate('paid'));

        $subscription->price = '25';
        $this->assertTrue($subscription->validate('price'));

        $subscription->price = null;
        $this->assertFalse($subscription->validate('price'));

        $subscription->start_time = date('Y-m-d H:i:s');
        $this->assertTrue($subscription->validate('start_time'));

        $subscription->end_time = date('Y-m-d H:i:s');
        $this->assertTrue($subscription->validate('end_time'));

        $subscription->create_time = date('Y-m-d H:i:s');
        $this->assertTrue($subscription->validate('create_time'));

        $subscription->update_time = date('Y-m-d H:i:s');
        $this->assertTrue($subscription->validate('update_time'));
    }

    public function testRelations()
    {
        $subscription = new Subscription();

        $this->assertInstanceOf(ActiveQuery::class, $subscription->getAdverts());
        $this->assertInstanceOf(ActiveQuery::class, $subscription->getUser());
    }

    public function testFindById()
    {
        /** @var Subscription $subscription */
        $subscription = Subscription::find()->orderBy('id DESC')->one();

        $this->assertInstanceOf(Subscription::class, Subscription::findById($subscription->id));
    }
}