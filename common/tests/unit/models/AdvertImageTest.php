<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Advert;
use common\models\AdvertImage;
use common\tests\UnitTester;
use yii\db\ActiveQuery;

class AdvertImageTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $advertImage = new AdvertImage();

        $advertImage->type = AdvertImage::TYPE_COVER;
        $this->assertTrue($advertImage->validate('type'));

        $advertImage->type = null;
        $this->assertFalse($advertImage->validate('type'));

        $advertImage->advert_id = 1;
        $this->assertTrue($advertImage->validate('advert_id'));

        $advertImage->advert_id = null;
        $this->assertFalse($advertImage->validate('advert_id'));

        $advertImage->name = 'AdvertImage';
        $this->assertTrue($advertImage->validate('name'));

        $advertImage->name = 123456;
        $this->assertFalse($advertImage->validate('name'));

        $advertImage->upload_time = date('Y-m-d H:i:s');
        $this->assertTrue($advertImage->validate('upload_time'));
    }

    public function testRelations()
    {
        $advertImage = new AdvertImage();

        $this->assertInstanceOf(ActiveQuery::class, $advertImage->getAdvert());
    }

    public function testFindByAdvertId()
    {
        /** @var Advert $advert */
        $advert = Advert::find()->orderBy('create_time DESC')->one();

        $this->assertInstanceOf(AdvertImage::class, AdvertImage::findByAdvertId($advert->id));
    }
}