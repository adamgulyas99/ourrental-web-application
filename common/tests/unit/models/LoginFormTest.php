<?php
namespace common\tests\unit\models;

use Codeception\Test\Unit;
use common\models\User;
use common\tests\UnitTester;
use common\models\LoginForm;

/**
 * Login form test
 */
class LoginFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testEmailValidation()
    {
        $loginForm = new LoginForm();

        $loginForm->email = 'ourrental.test@gmail.com';
        $this->assertTrue($loginForm->validate('email'));

        $loginForm->email = null;
        $this->assertFalse($loginForm->validate('email'));
    }

    public function testGetUser()
    {
        $loginForm = new LoginForm();

        $loginForm->email = 'ourrental.test@gmail.com';
        $this->assertInstanceOf(User::class, $loginForm->getUser());

        $loginForm->email = 'random.email@akarmi.hu';
        $this->assertNull($loginForm->getUser());
    }

    public function testLoginCorrect()
    {
        $loginForm = new LoginForm();

        $loginForm->email = 'ourrental.test@gmail.com';
        $loginForm->password = 'asd123456';
        $this->assertTrue($loginForm->login());
    }

    public function testLoginIncorrectEmail()
    {
        $loginForm = new LoginForm();

        $loginForm->email = 'random.email@akarmi.hu';
        $loginForm->password = 'asd123456';
        $this->assertFalse($loginForm->login());
    }

    public function testLoginIncorrectPassword()
    {
        $loginForm = new LoginForm();

        $loginForm->email = 'ourrental.test@gmail.com';
        $loginForm->password = 'random.wrong.pass';
        $this->assertFalse($loginForm->login());
    }
}
