<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Car;
use common\tests\UnitTester;
use yii\db\ActiveQuery;

class CarTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $car = new Car();

        $car->name = "Új bicikli";
        $this->assertTrue($car->validate('name'));

        $car->name = null;
        $this->assertFalse($car->validate('name'));

        $car->description = "Leírás";
        $this->assertTrue($car->validate('description'));

        $car->description = null;
        $this->assertFalse($car->validate('description'));

        $car->brand = "Capriolo";
        $this->assertTrue($car->validate('brand'));

        $car->brand = null;
        $this->assertFalse($car->validate('brand'));

        $car->type = Car::TYPE_SUV;
        $this->assertTrue($car->validate('type'));

        $car->type = null;
        $this->assertFalse($car->validate('type'));

        $car->age_group = 2015;
        $this->assertTrue($car->validate('age_group'));

        $car->age_group = 'Random text.';
        $this->assertFalse($car->validate('age_group'));

        $car->air_conditioner = Car::AIR_CONDITIONER_NO;
        $this->assertTrue($car->validate('air_conditioner'));

        $car->air_conditioner = 'Random text.';
        $this->assertFalse($car->validate('air_conditioner'));

        $car->create_time = date('Y-m-d H:i:s');
        $this->assertTrue($car->validate('create_time'));

        $car->update_time = date('Y-m-d H:i:s');
        $this->assertTrue($car->validate('update_time'));
    }

    public function testRelations()
    {
        $car = new Car();

        $this->assertInstanceOf(ActiveQuery::class, $car->getAdvert());
    }

    public function testConstructAdditionalData()
    {
        /** @var Car $car */
        $car = Car::find()->orderBy('id DESC')->one();

        $this->assertIsArray($car->constructAdditionalData());
    }

    public function testItemAlias()
    {
        $this->assertFalse(Car::itemAlias('randomCol'));

        $this->assertIsArray(Car::itemAlias('air_conditioner'));
        $this->assertIsString(Car::itemAlias('air_conditioner', Car::AIR_CONDITIONER_NO));

        $this->assertIsArray(Car::itemAlias('type'));
        $this->assertIsString(Car::itemAlias('type', Car::TYPE_SUV));
    }

    public function testFindById()
    {
        /** @var Car $car */
        $car = Car::find()->orderBy('create_time DESC')->one();

        $this->assertInstanceOf(Car::class, Car::findById($car->id));
    }
}
