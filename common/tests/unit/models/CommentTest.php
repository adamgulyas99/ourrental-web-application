<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Comment;
use common\tests\UnitTester;
use yii\db\ActiveQuery;

class CommentTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $comment = new Comment();

        $comment->user_id = 1;
        $this->assertTrue($comment->validate('user_id'));

        $comment->user_id = null;
        $this->assertFalse($comment->validate('user_id'));

        $comment->advert_id = 1;
        $this->assertTrue($comment->validate('advert_id'));

        $comment->advert_id = null;
        $this->assertFalse($comment->validate('advert_id'));

        $comment->subject = 'Random text.';
        $this->assertTrue($comment->validate('subject'));

        $comment->subject = null;
        $this->assertFalse($comment->validate('subject'));

        $comment->content = 'Random text.';
        $this->assertTrue($comment->validate('content'));

        $comment->content = null;
        $this->assertFalse($comment->validate('content'));
    }

    public function testRelations()
    {
        $comment = new Comment();

        $this->assertInstanceOf(ActiveQuery::class, $comment->getAdvert());
        $this->assertInstanceOf(ActiveQuery::class, $comment->getUser());
    }

    public function testFindById()
    {
        /** @var Comment $comment */
        $comment = Comment::find()->orderBy('id DESC')->one();

        $this->assertInstanceOf(Comment::class, Comment::findById($comment->id));
    }
}