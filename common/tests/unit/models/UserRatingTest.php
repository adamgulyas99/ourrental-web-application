<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\UserRating;
use common\tests\UnitTester;

class UserRatingTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $userRating = new UserRating();

        $userRating->create_user_id = 1;
        $this->assertTrue($userRating->validate('create_user_id'));

        $userRating->create_user_id = 'fdsgdfghdfg';
        $this->assertFalse($userRating->validate('create_user_id'));

        $userRating->advert_id = 1;
        $this->assertTrue($userRating->validate('advert_id'));

        $userRating->advert_id = 'fdsgdfghdfg';
        $this->assertFalse($userRating->validate('advert_id'));

        $userRating->rating = 1;
        $this->assertTrue($userRating->validate('rating'));

        $userRating->rating = 'fdsgdfghdfg';
        $this->assertFalse($userRating->validate('rating'));

        $userRating->create_time = date('Y-m-d H:i:s');
        $this->assertTrue($userRating->validate('create_time'));
    }
}