<?php
namespace unit\models;

use Codeception\Test\Unit;
use common\models\Advert;
use common\tests\UnitTester;
use yii\db\ActiveQuery;

class AdvertTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $advert = new Advert();

        $advert->type = Advert::TYPE_CAR;
        $this->assertTrue($advert->validate('type'));

        $advert->type = 123456789;
        $this->assertFalse($advert->validate('type'));

        $advert->deleted = true;
        $this->assertTrue($advert->validate('deleted'));

        $advert->deleted = 'asdff';
        $this->assertFalse($advert->validate('deleted'));

        $advert->user_id = 1;
        $this->assertTrue($advert->validate('user_id'));

        $advert->user_id = null;
        $this->assertFalse($advert->validate('user_id'));

        $advert->model_id = 1;
        $this->assertTrue($advert->validate('model_id'));

        $advert->model_id = null;
        $this->assertFalse($advert->validate('model_id'));

        $advert->rental_city = 'Szeged';
        $this->assertTrue($advert->validate('rental_city'));

        $advert->rental_city = null;
        $this->assertFalse($advert->validate('rental_city'));

        $advert->rental_start_time = date('Y-m-d H:i:s');
        $this->assertTrue($advert->validate('rental_start_time'));

        $advert->rental_start_time = null;
        $this->assertFalse($advert->validate('rental_start_time'));

        $advert->rental_end_time = date('Y-m-d H:i:s');
        $this->assertTrue($advert->validate('rental_end_time'));

        $advert->rental_end_time = null;
        $this->assertFalse($advert->validate('rental_end_time'));
    }

    public function testRelations()
    {
        $advert = new Advert();

        $this->assertInstanceOf(ActiveQuery::class, $advert->getAdvertImages());
        $this->assertInstanceOf(ActiveQuery::class, $advert->getAdvertCoverImage());
        $this->assertInstanceOf(ActiveQuery::class, $advert->getComments());
        $this->assertInstanceOf(ActiveQuery::class, $advert->getUser());
    }

    public function testItemAlias()
    {
        $this->assertFalse(Advert::itemAlias('randomCol'));
        $this->assertIsArray(Advert::itemAlias('type'));
        $this->assertIsString(Advert::itemAlias('type', Advert::TYPE_BICYCLE));
    }

    public function testSetDeleted()
    {
        $advert = new Advert();
        $advert->setDeleted();

        $this->assertTrue($advert->deleted);
    }

    public function testFindById()
    {
        /** @var Advert $advert */
        $advert = Advert::find()->orderBy('create_time DESC')->one();

        $this->assertInstanceOf(Advert::class, Advert::findById($advert->id));
    }

    public function testAddView()
    {
        $advert = new Advert();
        $advert->views = 0;
        $advert->addView();

        $this->assertTrue($advert->views === 1);
        $this->assertFalse($advert->views !== 1);
    }

    public function testSearch()
    {
        $this->assertInstanceOf(ActiveQuery::class, Advert::search(Advert::find()));
    }
}