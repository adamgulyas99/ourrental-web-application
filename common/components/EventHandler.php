<?php
namespace common\components;

use common\models\Advert;
use common\models\User;
use frontend\models\AdvertContact;
use yii\base\Event;
use Yii;

abstract class EventHandler
{
    /**
     * @param Event $event
     */
    public static function sendAdvertContactEmail(Event $event)
    {
        /** @var AdvertContact $advertContact */
        $advertContact = $event->sender;
        $advert = Advert::findById($advertContact->advert_id);

        $companyName = Yii::$app->params['companyName'];

        $message = "{$advertContact->name} ({$advertContact->email}) a következőt üzeni:\n" .
            $advertContact->message;

        Email::sendEmail(
            "{$companyName} - Valaki fel szeretné venni veled a kapcsolatot!",
            $advert->user->email,
            null,
            null,
            null,
            $message
        );
    }

    /**
     * @param Event $event
     */
    public static function sendAdvertCreationEmail(Event $event)
    {
        /** @var Advert $advert */
        $advert = $event->sender;
        $companyName = Yii::$app->params['companyName'];

        Email::sendEmail(
            "{$companyName} - Sikeres hirdetés létrehozás!",
            $advert->user->email,
            null,
            'advertCreate',
            ['addressee' => $advert->user->email]
        );
    }

    /**
     * @param Event $event
     */
    public static function sendRegistrationEmail(Event $event)
    {
        /** @var User $user */
        $user = $event->sender;
        $companyName = Yii::$app->params['companyName'];

        Email::sendEmail(
            "{$companyName} - Sikeres regisztráció!",
            $user->email,
            null,
            'registration',
            ['addressee' => $user->email]
        );
    }
}