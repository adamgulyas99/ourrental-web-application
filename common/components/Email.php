<?php
/** @noinspection PhpUndefinedFieldInspection */
/** @noinspection PhpUndefinedMethodInspection */

namespace common\components;

use Yii;

class Email
{
    /**
     * @param string $subject
     * @param string $to
     * @param string|null $from
     * @param string|null $view
     * @param array|null $params
     * @param null $text
     * @return string
     */
    public static function sendEmail(
        string $subject,
        string $to,
        $from = null,
        $view = null,
        $params = null,
        $text = null
    )
    {
        $message = empty($view) ? Yii::$app->mailerGmail->compose() : Yii::$app->mailerGmail->compose($view, $params);

        if (empty($from)) {
            $from = [Yii::$app->params['devEmail'] => "Ourrental Company"];
        }

        if (!YII_ENV_PROD) {
            $to = Yii::$app->params['devEmail'];
        }

        $message->setFrom($from)
            ->setTo($to)
            ->setSubject($subject);

        if (empty($view)) {
            $message->setTextBody($text);
        }

        return $message->send();
    }
}