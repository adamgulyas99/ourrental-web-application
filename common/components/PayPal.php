<?php
namespace common\components;

use yii\base\Component;

class PayPal extends Component
{
    public static function constructPaymentParams(array $params): array
    {
        return [
            'method' => 'paypal',
            'intent' => 'sale',
            'order' => [
                'description' => $params['name'],
                'subtotal' => $params['price'],
                'shippingCost' => 0,
                'total' => $params['price'],
                'currency' => 'EUR',
                'items' => [
                    [
                        'name' => $params['name'],
                        'price' => $params['price'],
                        'quantity' => 1,
                        'currency' => 'EUR',
                    ],
                ],
            ],
        ];
    }
}