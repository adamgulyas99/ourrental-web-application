<?php
namespace common\components;

class Country
{
    const AUSTRIA = 'AT';
    const CROATIA = 'HR';
    const HUNGARY = 'HU';
    const ROMANIA = 'RO';
    const SERBIA = 'RS';
    const SLOVAKIA = 'SK';
    const SLOVENIA = 'SI';
    const UKRAINE = 'UA';

    public static function getCountriesArray()
    {
        return [
            self::AUSTRIA => 'Ausztria',
            self::CROATIA => 'Horvátország',
            self::HUNGARY => 'Magyarország',
            self::ROMANIA => 'Románia',
            self::SERBIA => 'Szerbia',
            self::SLOVAKIA => 'Szlovákia',
            self::SLOVENIA => 'Szlovénia',
            self::UKRAINE => 'Ukrajna',
        ];
    }
}