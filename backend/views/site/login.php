<?php

/* @var View $this */
/* @var ActiveForm $form */
/* @var LoginForm $loginForm */

use common\models\LoginForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Bejelentkezés';
?>
<div class="text-center">
    <h2><?= $this->title ?></h2>
</div>

<div class="row justify-content-center">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($loginForm, 'email')->textInput(['autofocus' => true]) ?>

        <?= $form->field($loginForm, 'password')->passwordInput() ?>

        <div class="form-group text-center">
            <?= Html::submitButton('Bejelentkezés', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
