<?php
/** @noinspection PhpUnhandledExceptionInspection */

/** @var View $this */
/** @var UserSearch $userSearch */
/** @var ActiveDataProvider $dataProvider */

use backend\models\UserSearch;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::t('app', 'Felhasználók');
?>
<div class="text-center mb-3">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $userSearch,
    'columns' => [
        [
            'attribute' => 'id',
            'filter' => Html::activeTextInput($userSearch, 'id', ['class' => 'form-control']),
            'label' => $userSearch->getAttributeLabel('id'),
            'value' => function ($data) {
                /** @var UserSearch $data */
                return '#' . $data->id;
            }
        ],
        [
            'attribute' => 'email',
            'filter' => Html::activeTextInput($userSearch, 'email', ['class' => 'form-control']),
            'label' => $userSearch->getAttributeLabel('email'),
        ],
        [
            'attribute' => 'hasSubscription',
            'filter' => Html::activeTextInput($userSearch, 'hasSubscription', ['class' => 'form-control']),
            'label' => $userSearch->getAttributeLabel('hasSubscription'),
            'value' => function ($data) {
                /** @var UserSearch $data */
                return !empty($data->subscriptions) ? '#' . $data->subscriptions[0]->id : '-';
            }
        ],
        [
            'attribute' => 'advertCount',
            'filter' => false,
            'label' => $userSearch->getAttributeLabel('advertCount'),
            'value' => function ($data) {
                /** @var UserSearch $data */
                return !empty($data->adverts) ? count($data->adverts) : 0;
            }
        ],
        [
            'attribute' => 'is_admin',
            'filter' => Html::activeDropDownList(
                $userSearch,
                'is_admin',
                [User::IS_ADMIN_TRUE => 'Igen', User::IS_ADMIN_FALSE => 'Nem'],
                ['class' => 'form-control', 'prompt' => '']
            ),
            'label' => $userSearch->getAttributeLabel('is_admin'),
            'value' => function ($data) {
                /** @var UserSearch $data */
                return (bool)$data->is_admin === true ? 'Igen' : 'Nem';
            }
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{delete} {login-as}',
            'buttons' => [
                'delete' => function($url, $user) {
                    /** @var User $user */
                    return Html::button('<i class="fa fa-times p-1"></i>', [
                        'class' => 'border-0 bg-transparent',
                        'data-target' => '#modal',
                        'data-toggle' => 'modal',
                        'data-id' => $user->id,
                        'title' => 'Felhasználó törlése',
                    ]);
                },
                'login-as' => function($url, $model) {
                    return Html::a('<i class="fa fa-sign-in-alt p-1"></i>', $url, ['title' => 'Átjelentkezés']);
                },
            ],
        ],
    ],
]) ?>

<?= $this->render('_delete') ?>
