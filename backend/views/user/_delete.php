<?php
/** @var User $user */

use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Modal;

$buttons = Html::button('Törlés', [
    'type' => 'submit',
    'class' => 'btn btn-danger',
    'form' => 'user-delete-form',
]);
$buttons .= Html::button('Mégse', [
    'type' => 'button',
    'class' => 'btn btn-outline-secondary',
    'data-dismiss' => 'modal'
]);

Modal::begin([
    'id' => 'modal',
    'footer' => $buttons,
    'footerOptions' => ['class' => 'justify-content-center'],
    'headerOptions' => ['id' => 'modalHeader'],
    'size' => 'modal-lg',
    'title' => 'Felhasználó törlése',
]);
?>

<form action="<?= Url::to(['delete']) ?>" class="form-horizontal" id="user-delete-form" method="get">
    <div class="text-center">
        <p>Biztosan törlöd a kiválasztott felhasználót?</p>
    </div>

    <?= Html::hiddenInput('id') ?>
</form>

<?php Modal::end(); ?>