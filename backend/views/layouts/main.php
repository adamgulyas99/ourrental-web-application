<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\models\User;
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

$user = null;
if (!Yii::$app->user->isGuest) {
    $user = User::findById(Yii::$app->user->id);
}

$leftMenuItems = [];
$rightMenuItems = [];

$leftMenuItems[] = Html::a('Felhasználók', Url::to('/user/index'), ['class' => 'text-light text-decoration-none']);
$leftMenuItems[] = Html::a('Előfizetések', Url::to('/subscription/index'), ['class' => 'text-light text-decoration-none']);
$leftMenuItems[] = Html::a('Hirdetések', Url::to('/advert/index'), ['class' => 'text-light text-decoration-none']);

if (Yii::$app->user->isGuest) {
    $rightMenuItems[] = Html::a('Belépés', Url::to('/site/login'), ['class' => 'text-light text-decoration-none']);
} else {
    $rightMenuItems[] = Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            '<i class="fa fa-sign-out-alt"></i>',
            [
                'class' => 'btn btn-md-dark text-light logout text-decoration-none p-0',
                'title' => $user->email,
            ]
        )
        . Html::endForm();
}

$this->registerLinkTag(['rel' => 'icon', 'type' => 'png', 'href' => Url::to('@web/img/logo_our_rental_large.png')]);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link href="<?= Url::to('@web/assets/font-awesome/fontawesome-free-5.14.0-web/css/all.css') ?>" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap p-0">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark p-0">
        <div class="container">
            <div class="navbar-brand mr-4">
                <?= Html::tag('img', '', ['src' => Url::to('/img/logo_our_rental_large.png'), 'alt' => 'OurRental', 'width' => 64, 'height' => 34]) ?>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Lenyíló menű">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav mr-auto">
                    <?php foreach ($leftMenuItems as $leftMenuItem) { ?>
                        <li class="nav-item btn btn-dark text-sm-right mb-1 h4">
                            <?= $leftMenuItem ?>
                        </li>
                    <?php } ?>
                </ul>

                <ul class="navbar-nav">
                    <?php foreach ($rightMenuItems as $rightMenuItem) { ?>
                        <li class="nav-item btn btn-dark text-sm-right mb-1 h4">
                            <?= $rightMenuItem ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container" style="padding: 125px 0 !important;">
        <?php if (Yii::$app->session->hasFlash('success')) { ?>
            <?= $this->render('_alert', ['key' => 'success']) ?>
        <?php } ?>

        <?php if (Yii::$app->session->hasFlash('danger')) { ?>
            <?= $this->render('_alert', ['key' => 'danger']) ?>
        <?php } ?>

        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="text-center">
        <p>&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
