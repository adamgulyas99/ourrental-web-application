<?php
/** @noinspection PhpUnhandledExceptionInspection */

/** @var string $key */

use \yii\bootstrap4\Alert;

$flashMessage = Yii::$app->session->getFlash($key);

echo Alert::widget([
    'options' => [
        'class' => 'alert-' . $key
    ],
    'body' => $flashMessage,
]);