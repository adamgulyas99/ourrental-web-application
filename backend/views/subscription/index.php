<?php
/** @noinspection PhpUnhandledExceptionInspection */

/** @var View $this */
/** @var SubscriptionSearch $subscriptionSearch */
/** @var ActiveDataProvider $dataProvider */

use backend\models\SubscriptionSearch;
use common\models\Subscription;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('app', 'Előfizetések');
?>
<div class="text-center mb-3">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $subscriptionSearch,
    'columns' => [
        [
            'attribute' => 'id',
            'filter' => Html::activeTextInput($subscriptionSearch, 'id', ['class' => 'form-control']),
            'label' => $subscriptionSearch->getAttributeLabel('id'),
            'value' => function ($data) {
                /** @var SubscriptionSearch $data */
                return '#' . $data->id;
            }
        ],
        [
            'attribute' => 'userEmail',
            'filter' => Html::activeTextInput($subscriptionSearch, 'userEmail', ['class' => 'form-control']),
            'label' => $subscriptionSearch->getAttributeLabel('userEmail'),
            'value' => function ($data) {
                /** @var SubscriptionSearch $data */
                return $data->user->email;
            },
        ],
        [
            'attribute' => 'name',
            'filter' => Html::activeTextInput($subscriptionSearch, 'name', ['class' => 'form-control']),
            'label' => $subscriptionSearch->getAttributeLabel('name'),
        ],
        [
            'attribute' => 'paid',
            'filter' => Html::activeDropDownList(
                $subscriptionSearch,
                'paid',
                [true => 'Igen', false => 'Nem'],
                ['class' => 'form-control', 'prompt' => '']
            ),
            'label' => $subscriptionSearch->getAttributeLabel('paid'),
            'value' => function ($data) {
                /** @var SubscriptionSearch $data */
                return $data->paid ? 'Igen' : 'Nem';
            }
        ],
        [
            'label' => 'Aktív',
            'value' => function ($data) {
                /** @var SubscriptionSearch $data */
                return strtotime($data->end_time) > time() ? 'Igen' : 'Nem';
            }
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{delete} {edit}',
            'buttons' => [
                'delete' => function ($url, $subscription) {
                    /** @var Subscription $subscription */

                    return Html::button('<i class="fa fa-times"></i>', [
                        'class' => 'border-0 bg-transparent',
                        'data-target' => '#modal',
                        'data-toggle' => 'modal',
                        'data-id' => $subscription->id,
                        'title' => 'Előfizetés törlése',
                    ]);
                },
                'edit' => function ($url, $subscription) {
                    /** @var Subscription $subscription */

                    return Html::a('<i class="fa fa-edit"></i>', Url::to(['update', 'id' => $subscription->id]), [
                        'class' => 'border-0 bg-transparent',
                        'title' => 'Előfizetés módosítása',
                    ]);
                }
            ],
        ],
    ],
]) ?>

<?= $this->render('_delete') ?>
