<?php
/** @var SubscriptionForm $subscriptionForm */

use backend\models\SubscriptionForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$this->title = "Előfizetés módosítása";
?>
<div class="text-center">
    <h2><?= $this->title ?></h2>
</div>

<div class="row justify-content-center">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'update-subscription-form']); ?>

        <?= $form->field($subscriptionForm, 'endTime')->textInput(['autofocus' => true, 'placeholder' => '2020-01-01 00:00:00']) ?>

        <div class="form-group text-center">
            <?= Html::submitButton('Mentés', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
