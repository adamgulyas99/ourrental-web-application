<?php
/** @var AdvertForm $advertForm */

use backend\models\AdvertForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$this->title = "Hirdetés módosítása";
?>
<div class="text-center">
    <h2><?= $this->title ?></h2>
</div>

<div class="row justify-content-center">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'update-advert-form']); ?>

        <?= $form->field($advertForm, 'title')->textInput(['autofocus' => true]) ?>
        <?= $form->field($advertForm, 'description')->textarea() ?>
        <?= $form->field($advertForm, 'type')->dropDownList(AdvertForm::itemAlias('type')) ?>
        <?= $form->field($advertForm, 'rentalPrice')->textInput() ?>
        <?= $form->field($advertForm, 'rentalCity')->textInput() ?>
        <?= $form->field($advertForm, 'rentalStartTime')->textInput() ?>
        <?= $form->field($advertForm, 'rentalEndTime')->textInput() ?>

        <div class="form-group text-center">
            <?= Html::submitButton('Mentés', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
