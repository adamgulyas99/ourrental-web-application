<?php
/** @noinspection PhpUnhandledExceptionInspection */

/** @var View $this */
/** @var AdvertSearch $advertSearch */
/** @var ActiveDataProvider $dataProvider */

use backend\models\AdvertForm;
use backend\models\AdvertSearch;
use common\models\Advert;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('app', 'Hirdetések');
?>
<div class="text-center mb-3">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $advertSearch,
    'columns' => [
        [
            'attribute' => 'id',
            'filter' => Html::activeTextInput($advertSearch, 'id', ['class' => 'form-control']),
            'label' => $advertSearch->getAttributeLabel('id'),
            'value' => function ($data) {
                /** @var AdvertSearch $data */
                return '#' . $data->id;
            }
        ],
        [
            'attribute' => 'userEmail',
            'filter' => Html::activeTextInput($advertSearch, 'userEmail', ['class' => 'form-control']),
            'label' => $advertSearch->getAttributeLabel('userEmail'),
            'value' => function ($data) {
                /** @var AdvertSearch $data */
                return $data->user->email;
            },
        ],
        [
            'attribute' => 'type',
            'filter' => Html::activeDropDownList(
                $advertSearch,
                'type',
                AdvertForm::itemAlias('type'),
                ['class' => 'form-control', 'prompt' => '']
            ),
            'label' => $advertSearch->getAttributeLabel('type'),
            'value' => function ($data) {
                /** @var AdvertSearch $data */
                return AdvertForm::itemAlias('type', $data->type);
            }
        ],
        [
            'attribute' => 'title',
            'filter' => Html::activeTextInput($advertSearch, 'title', ['class' => 'form-control']),
            'label' => $advertSearch->getAttributeLabel('title'),
        ],
        [
            'attribute' => 'isActive',
            'filter' => Html::activeDropDownList(
                $advertSearch,
                'isActive',
                [false => 'Igen', true => 'Nem'],
                ['class' => 'form-control', 'prompt' => '']
            ),
            'label' => $advertSearch->getAttributeLabel('isActive'),
            'value' => function ($data) {
                /** @var AdvertSearch $data */
                return !$data->deleted ? 'Igen' : 'Nem';
            }
        ],
        [
            'attribute' => 'views',
            'label' => $advertSearch->getAttributeLabel('views'),
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{delete} {edit}',
            'buttons' => [
                'delete' => function ($url, $advert) {
                    /** @var Advert $advert */
                    return Html::button('<i class="fa fa-times"></i>', [
                        'class' => 'border-0 bg-transparent',
                        'data-target' => '#modal',
                        'data-toggle' => 'modal',
                        'data-id' => $advert->id,
                        'title' => 'Hirdetés törlése',
                    ]);
                },
                'edit' => function ($url, $advert) {
                    /** @var Advert $advert */

                    return Html::a('<i class="fa fa-edit"></i>', Url::to(['update', 'id' => $advert->id]), [
                        'class' => 'border-0 bg-transparent',
                        'title' => 'Hirdetés módosítása',
                    ]);
                }
            ],
        ],
    ],
]) ?>

<?= $this->render('_delete') ?>
