<?php
namespace backend\unit\models;

use backend\models\AdvertForm;
use Codeception\Test\Unit;
use common\models\Advert;
use common\tests\UnitTester;

class AdvertFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $advertForm = new AdvertForm();

        $advertForm->title = 'Cím';
        $this->assertTrue($advertForm->validate('title'));

        $advertForm->title = 123456;
        $this->assertFalse($advertForm->validate('title'));

        $advertForm->description = 'Leírás';
        $this->assertTrue($advertForm->validate('description'));

        $advertForm->description = 123456;
        $this->assertFalse($advertForm->validate('description'));

        $advertForm->type = Advert::TYPE_BICYCLE;
        $this->assertTrue($advertForm->validate('type'));

        $advertForm->type = null;
        $this->assertFalse($advertForm->validate('type'));

        $advertForm->rentalCity = 'Város';
        $this->assertTrue($advertForm->validate('rentalCity'));

        $advertForm->rentalCity = 123456;
        $this->assertFalse($advertForm->validate('rentalCity'));

        $advertForm->rentalPrice = '123456';
        $this->assertTrue($advertForm->validate('rentalPrice'));

        $advertForm->rentalPrice = 123456;
        $this->assertFalse($advertForm->validate('rentalPrice'));

        $advertForm->rentalStartTime = '2020-02-03';
        $this->assertTrue($advertForm->validate('rentalStartTime'));

        $advertForm->rentalStartTime = 123456;
        $this->assertFalse($advertForm->validate('rentalStartTime'));

        $advertForm->rentalEndTime = '2020-02-03';
        $this->assertTrue($advertForm->validate('rentalEndTime'));

        $advertForm->rentalEndTime = 123456;
        $this->assertFalse($advertForm->validate('rentalEndTime'));
    }

    public function testFillFrom()
    {
        /** @var Advert $advert */
        $advert = Advert::find()->orderBy('id')->one();
        $advertForm = new AdvertForm();
        $advertForm->fillFrom($advert);

        $this->assertTrue($advertForm->validate());
    }

    public function testFillTo()
    {
        /** @var Advert $advert */
        /** @var AdvertForm $advertForm */

        $advert = Advert::find()->orderBy('id')->one();
        $advertForm = new AdvertForm();

        $this->assertInstanceOf(Advert::class, $advertForm->fillTo($advert));
    }

    public function testItemAlias()
    {
        $this->assertFalse(AdvertForm::itemAlias('randomCol'));
        $this->assertIsArray(AdvertForm::itemAlias('type'));
        $this->assertIsString(AdvertForm::itemAlias('type', Advert::TYPE_BICYCLE));
    }
}