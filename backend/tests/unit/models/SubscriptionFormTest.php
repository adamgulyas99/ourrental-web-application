<?php
namespace backend\unit\models;

use backend\models\SubscriptionForm;
use Codeception\Test\Unit;
use common\models\Subscription;
use common\tests\UnitTester;

class SubscriptionFormTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testValidation()
    {
        $subscriptionForm = new SubscriptionForm();

        $subscriptionForm->endTime = '2020-10-02 10:00:00';
        $this->assertTrue($subscriptionForm->validate('endTime'));

        $subscriptionForm->endTime = null;
        $this->assertFalse($subscriptionForm->validate('endTime'));

        $subscriptionForm->name = 'asd';
        $this->assertTrue($subscriptionForm->validate('name'));

        $subscriptionForm->name = 123456;
        $this->assertFalse($subscriptionForm->validate('name'));

        $subscriptionForm->paid = true;
        $this->assertTrue($subscriptionForm->validate('paid'));

        $subscriptionForm->price = '120000';
        $this->assertTrue($subscriptionForm->validate('price'));

        $subscriptionForm->price = null;
        $this->assertFalse($subscriptionForm->validate('price'));

        $subscriptionForm->startTime = '2020-10-02 10:00:00';
        $this->assertTrue($subscriptionForm->validate('startTime'));

        $subscriptionForm->updateTime = '2020-10-02 10:00:00';
        $this->assertTrue($subscriptionForm->validate('updateTime'));

        $subscriptionForm->userId = 1;
        $this->assertTrue($subscriptionForm->validate('userId'));

        $subscriptionForm->userId = null;
        $this->assertFalse($subscriptionForm->validate('userId'));
    }

    public function testFillFrom()
    {
        /** @var Subscription $subscription */
        $subscription = Subscription::find()->orderBy('id')->one();
        $subscriptionForm = new SubscriptionForm();
        $subscriptionForm->fillFrom($subscription);

        $this->assertTrue($subscriptionForm->validate());
    }

    public function testFillTo()
    {
        /** @var Subscription $subscription */
        /** @var SubscriptionForm $subscriptionForm */

        $subscription = Subscription::find()->orderBy('id')->one();
        $subscriptionForm = new SubscriptionForm();

        $this->assertInstanceOf(Subscription::class, $subscriptionForm->fillTo($subscription));
    }
}