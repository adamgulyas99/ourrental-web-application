<?php
namespace backend\controllers;

use backend\models\SubscriptionForm;
use backend\models\SubscriptionSearch;
use common\models\Subscription;
use Throwable;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;

class SubscriptionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['delete', 'index', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionDelete(int $id)
    {
        $subscription = Subscription::findById($id);
        if (strtotime($subscription->end_time) > time()) {
            Yii::$app->session->setFlash('danger', 'Aktív előfizetés nem törölhető!');

            return $this->redirect('index');
        }

        try {
            $subscription->delete();
        } catch (Throwable $e) {
            Yii::error($e);
        }

        Yii::$app->session->setFlash('success', 'Sikeres előfizetés törlés.');

        return $this->redirect(Yii::$app->request->referrer ?? Yii::$app->homeUrl);
    }

    public function actionIndex()
    {
        $subscriptionSearch = new SubscriptionSearch();

        $dataProvider = $subscriptionSearch->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 25;

        return $this->render('index', [
            'subscriptionSearch' => $subscriptionSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate(int $id)
    {
        $subscription = Subscription::findById($id);
        $subscriptionForm = new SubscriptionForm();

        $subscriptionForm->fillFrom($subscription);

        if ($subscriptionForm->load(Yii::$app->request->post()) && $subscriptionForm->validate()) {
            $subscription = $subscriptionForm->fillTo($subscription);

            try {
                if (!$subscription->save()) {
                    throw new Exception(
                        "Failed to save #{$subscription->id} Subscription. " . json_encode($subscription->getErrors())
                    );
                }

                Yii::$app->session->setFlash('success', 'Sikeres előfizetés módosítás.');
            } catch (\Throwable $e) {
                Yii::error($e);
                Yii::$app->session->setFlash('danger', 'Sikertelen előfizetés módosítás.');
            }

            return $this->redirect(['index']);
        }

        return $this->render('update', ['subscriptionForm' => $subscriptionForm]);
    }
}