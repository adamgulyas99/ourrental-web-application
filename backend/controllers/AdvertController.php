<?php
/** @noinspection DuplicatedCode */

namespace backend\controllers;

use backend\models\AdvertForm;
use common\models\Advert;
use backend\models\AdvertSearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use Throwable;
use Yii;

class AdvertController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['delete', 'index', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionDelete(int $id)
    {
        $advert = Advert::findById($id);

        try {
            $advert->setDeleted();

            if (!$advert->save()) {
                throw new \Exception("Failed to set deleted #{$advert->id} advert.");
            }
        } catch (Throwable $e) {
            Yii::error($e);
        }

        Yii::$app->session->setFlash('success', 'Sikeres előfizetés törlés.');

        return $this->redirect(Yii::$app->request->referrer ?? Yii::$app->homeUrl);
    }

    public function actionIndex()
    {
        $advertSearch = new AdvertSearch();

        $dataProvider = $advertSearch->adminSearch(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 25;

        return $this->render('index', [
            'advertSearch' => $advertSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate(int $id)
    {
        $advert = Advert::findById($id);
        $advertForm = new AdvertForm();

        $advertForm->fillFrom($advert);

        if ($advertForm->load(Yii::$app->request->post())) {
            $advert = $advertForm->fillTo($advert);

            try {
                if (!$advert->save()) {
                    throw new Exception(
                        "Failed to save #{$advert->id} Subscription. " . json_encode($advert->getErrors())
                    );
                }

                Yii::$app->session->setFlash('success', 'Sikeres hirdetés módosítás.');
            } catch (\Throwable $e) {
                Yii::error($e);
                Yii::$app->session->setFlash('danger', 'Sikertelen hirdetés módosítás.');
            }

            return $this->redirect(['index']);
        }

        return $this->render('update', ['advertForm' => $advertForm]);
    }
}