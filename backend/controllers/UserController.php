<?php
/** @noinspection PhpUndefinedFieldInspection */

namespace backend\controllers;

use common\models\User;
use backend\models\UserSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use Throwable;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['delete', 'index', 'login-as'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionDelete(int $id)
    {
        $user = User::findById($id);
        if ($user->is_admin) {
            Yii::$app->session->setFlash('danger', 'Admin nem törölhető!');

            return $this->redirect('index');
        }

        try {
            $user->delete();
        } catch (Throwable $e) {
            Yii::error($e);
        }

        Yii::$app->session->setFlash('success', 'Sikeres felhasználó törlés.');

        return $this->redirect(Yii::$app->request->referrer ?? Yii::$app->homeUrl);
    }

    public function actionIndex()
    {
        $userSearch = new UserSearch();

        $dataProvider = $userSearch->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 25;

        return $this->render('index', [
            'userSearch' => $userSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLoginAs(int $id)
    {
        return $this->redirect(Yii::$app->urlManagerFrontend->createUrl(['/login-as', 'id' => $id]));
    }
}