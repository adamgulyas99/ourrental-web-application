<?php
namespace backend\models;

use common\models\Subscription;
use Yii;
use yii\data\ActiveDataProvider;

class SubscriptionSearch extends Subscription
{
    /** @var int */
    public $id;
    /** @var string */
    public $userEmail;
    /** @var string */
    public $name;
    /** @var bool */
    public $paid;
    /** @var string */
    public $endTime;

    public function rules(): array
    {
        return [
            ['id', 'integer'],
            ['userEmail', 'email'],
            [['endTime', 'name'], 'string'],
            ['paid', 'boolean'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app', 'Kód'),
            'userEmail' => Yii::t('app', 'Felhasználó email'),
            'name' => Yii::t('app', 'Csomag név'),
            'paid' => Yii::t('app', 'Fizetve'),
        ];
    }

    public function search(array $params): ActiveDataProvider
    {
        $query = Subscription::find();
        $query->alias('s');
        $query->joinWith(['user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'userEmail' => [
                        'asc' => ['user.email' => SORT_ASC, 'id' => SORT_ASC],
                        'desc' => ['user.email' => SORT_DESC, 'id' => SORT_DESC],
                    ],
                    'name',
                    'paid',
                ],
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
        ]);

        if (!$this->load($params) && !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['name' => $this->name]);
        $query->andFilterWhere(['paid' => $this->paid]);

        return $dataProvider;
    }
}