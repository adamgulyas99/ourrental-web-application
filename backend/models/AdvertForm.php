<?php
/** @noinspection DuplicatedCode */

namespace backend\models;

use common\models\Advert;
use common\models\Bicycle;
use common\models\Car;
use common\models\Motorbike;
use \yii\web\User;
use Yii;
use yii\base\Model;

class AdvertForm extends Model
{
    /** @var string */
    public $title;
    /** @var string */
    public $description;
    /** @var int */
    public $type;
    /** @var string */
    public $rentalPrice;
    /** @var string */
    public $rentalCity;
    /** @var string */
    public $rentalStartTime;
    /** @var string */
    public $rentalEndTime;

    public function rules(): array
    {
        return [
            ['type', 'integer', 'max' => 3],
            ['title', 'string', 'max' => 255],

            [['type'], 'required'],
            [['description', 'rentalPrice', 'rentalCity', 'rentalStartTime', 'rentalEndTime'], 'string'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'userId' => Yii::t('app', 'Felhasználó ID'),
            'modelId' => Yii::t('app', 'Model ID'),
            'title' => Yii::t('app', 'Cím'),
            'description' => Yii::t('app', 'Leírás'),
            'type' => Yii::t('app', 'Típus'),
            'data' => Yii::t('app', 'Adat'),
            'rentalPrice' => Yii::t('app', 'Bérlet ár'),
            'rentalCity' => Yii::t('app', 'Város'),
            'rentalStartTime' => Yii::t('app', 'Bérlet kezdete'),
            'rentalEndTime' => Yii::t('app', 'Bérlet vége'),
        ];
    }

    public function fillFrom(Advert $advert)
    {
        $this->description = $advert->description;
        $this->title = $advert->title;
        $this->type = $advert->type;

        $this->rentalCity = $advert->rental_city;
        $this->rentalEndTime = $advert->rental_end_time;
        $this->rentalPrice = $advert->rental_price;
        $this->rentalStartTime = $advert->rental_start_time;
    }

    public function fillTo(Advert $advert): Advert
    {
        $advert->description = $this->description;
        $advert->title = $this->title;
        $advert->type = $this->type;
        $advert->update_time = date('Y-m-d H:i:s');

        $advert->rental_city = $this->rentalCity;
        $advert->rental_end_time = $this->rentalEndTime;
        $advert->rental_price = $this->rentalPrice;
        $advert->rental_start_time = $this->rentalStartTime;

        return $advert;
    }

    /**
     * @param string $col
     * @param null|int $value
     * @return bool|mixed
     */
    public static function itemAlias(string $col, $value = null)
    {
        $result = [
            'type' => [
                Advert::TYPE_BICYCLE => 'Bicikli',
                Advert::TYPE_MOTORBIKE => 'Motor',
                Advert::TYPE_CAR => 'Autó',
            ],
        ];

        if (isset($value)) {
            return isset($result[$col][$value]) ? $result[$col][$value] : false;
        }

        return isset($result[$col]) ? $result[$col] : false;
    }
}