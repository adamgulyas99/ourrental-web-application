<?php
/** @noinspection DuplicatedCode */

namespace backend\models;

use Yii;
use common\models\Subscription;
use yii\base\Model;

class SubscriptionForm extends Model
{
    /** @var string */
    public $endTime;
    /** @var string */
    public $name;
    /** @var bool */
    public $paid;
    /** @var string */
    public $price;
    /** @var string */
    public $startTime;
    /** @var string */
    public $updateTime;
    /** @var int */
    public $userId;

    public function rules(): array
    {
        return [
            ['paid', 'boolean'],
            ['userId', 'integer'],

            [['userId', 'name', 'paid', 'price', 'endTime'], 'required'],
            [['name', 'price'], 'string', 'max' => 255],
            [['startTime', 'endTime', 'updateTime'], 'safe'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'endTime' => Yii::t('app', 'Vége'),
            'name' => Yii::t('app', 'Elnevezés'),
            'paid' => Yii::t('app', 'Fizetett'),
            'price' => Yii::t('app', 'Ár'),
            'startTime' => Yii::t('app', 'Kezdete'),
            'updateTime' => Yii::t('app', 'Módosítás dátuma'),
            'userId' => Yii::t('app', 'Felhasználó ID'),
        ];
    }

    public function fillFrom(Subscription $subscription)
    {
        $this->endTime = $subscription->end_time;
        $this->name = $subscription->name;
        $this->paid = $subscription->paid;
        $this->price = $subscription->price;
        $this->startTime = $subscription->start_time;
        $this->updateTime = $subscription->update_time;
        $this->userId = $subscription->user_id;
    }

    public function fillTo(Subscription $subscription): Subscription
    {
        $subscription->end_time = $this->endTime;
        $subscription->start_time = $this->startTime;
        $subscription->update_time = date('Y-m-d H:i:s');

        return $subscription;
    }
}