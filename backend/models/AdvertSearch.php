<?php
namespace backend\models;

use common\models\Advert;
use yii\data\ActiveDataProvider;
use Yii;

class AdvertSearch extends Advert
{
    /** @var int */
    public $id;
    /** @var string */
    public $userEmail;
    /** @var int */
    public $type;
    /** @var string */
    public $title;
    /** @var bool */
    public $isActive;
    /** @var int */
    public $views;

    public function rules(): array
    {
        return [
            [['id', 'type', 'views'], 'integer'],
            ['userEmail', 'email'],
            ['title', 'string'],
            ['isActive', 'boolean'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app', 'Kód'),
            'isActive' => Yii::t('app', 'Aktív'),
            'title' => Yii::t('app', 'Elnevezés'),
            'type' => Yii::t('app', 'Típus'),
            'userEmail' => Yii::t('app', 'Felhasználó email'),
            'views' => Yii::t('app', 'Látogatottság'),
        ];
    }

    public function adminSearch(array $params): ActiveDataProvider
    {
        $query = Advert::find();
        $query->alias('a');
        $query->joinWith(['user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'userEmail' => [
                        'asc' => ['user.email' => SORT_ASC, 'id' => SORT_ASC],
                        'desc' => ['user.email' => SORT_DESC, 'id' => SORT_DESC],
                    ],
                    'type',
                    'title',
                    'views',
                ],
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
        ]);

        if (!$this->load($params) && !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['user.email' => $this->userEmail]);
        $query->andFilterWhere(['type' => $this->type]);
        $query->andFilterWhere(['deleted' => $this->isActive]);
        $query->andFilterWhere(['title' => $this->title]);
        $query->andFilterWhere(['views' => $this->views]);

        return $dataProvider;
    }
}