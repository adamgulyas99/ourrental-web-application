<?php
namespace backend\models;

use common\models\User;
use yii\data\ActiveDataProvider;
use Yii;

class UserSearch extends User
{
    /** @var int */
    public $id;
    /** @var string */
    public $email;
    /** @var bool */
    public $is_admin;

    /** @var bool */
    public $hasSubscription;
    /** @var int */
    public $advertCount;

    public function rules()
    {
        return [
            ['email', 'email'],
            [['id', 'advertCount'], 'integer'],
            [['is_admin', 'hasSubscription'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Kód'),
            'email' => Yii::t('app', 'Email'),
            'is_admin' => Yii::t('app', 'Admin'),
            'advertCount' => Yii::t('app', 'Hirdetések száma'),
            'hasSubscription' => Yii::t('app', 'Csomag'),
        ];
    }

    public function search(array $params): ActiveDataProvider
    {
        $query = User::find();
        $query->alias('u');
        $query->leftJoin('subscription s', 'u.id = s.user_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'email' => [
                        'asc' => ['email' => SORT_ASC, 'id' => SORT_ASC],
                        'desc' => ['email' => SORT_DESC, 'id' => SORT_DESC],
                    ],
                    'is_admin',
                    'hasSubscription' => [
                        'asc' => ['s.id' => SORT_ASC],
                        'desc' => ['s.id' => SORT_DESC],
                    ],
                ],
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
        ]);

        if (!$this->load($params) && !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['email' => $this->email]);
        $query->andFilterWhere(['is_admin' => $this->is_admin]);
        $query->andFilterWhere(['s.id' => $this->hasSubscription]);

        return $dataProvider;
    }
}