**Az applikáció installálása:**

Szükséges software-ek:
1. VirtualBox
2. Vagrant
3. GitHub Token Generálása

Lépések:
1. Klónozd a repository-t: `git clone https://gitlab.com/adamgulyas99/ourrental-web-application.git`
2. Lépj be a vagrant config folderhez: `cd vagrant/config`
3. Hozd létre a vagrant-local.yml-t: `cp vagrant-local.example.yml vagrant-local.yml`
4. Add hozzá a megfelelő helyre a GitHub tokened a vagrant-local.yml-ben
5. Lépj vissza a root-ba: `cd ../..`
6. Indítsd el a vagrant-ot: `vagrant up`
7. SSH a vagrant gépre: `vagrant ssh`
8. Lépj be az app root-ba: `cd /app`
9. Futtasd a migrációkat: `./yii migrate`

Ügyfél oldali URL: [ourrental.test](http://ourrental.test)<br>
Admin oldali URL: [ourrental-admin.test](http://ourrental-admin.test)